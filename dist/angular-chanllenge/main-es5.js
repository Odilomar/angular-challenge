function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<ng-container>\r\n    <div style=\"position: relative;\">\r\n        <ngx-ui-loader></ngx-ui-loader>\r\n    </div>\r\n    <router-outlet></router-outlet>\r\n</ng-container>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/film/film.component.html":
  /*!********************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/film/film.component.html ***!
    \********************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreLayoutFilmFilmComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container px-0 px-sm-0 px-md-auto px-lg-auto px-xl-auto\" *ngIf=\"film\">\r\n    <div class=\"row\">\r\n        <div class=\"col-12 text-center text-sm-center text-md-left text-lg-left text-xl-left\">\r\n            <h5 class=\"mb-0\">{{film.title | titlecase}}</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <hr *ngIf=\" film.title !='' \">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>General</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"film.title != ''\">\r\n        <div class=\"col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n            <p><strong>Title:</strong> {{film.title | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n            <p><strong>Episode:</strong> {{film.episode_id}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4\">\r\n            <p><strong>Director:</strong> {{film.director | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\r\n            <p><strong>Producer:</strong> {{film.producer | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-6 col-xl-6\">\r\n            <p><strong>Release Date:</strong> {{film.release_date | titlecase | date:\"dd/MM/yyyy\"}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12\">\r\n            <p class=\"text-justify\"><strong>Opening Crawl:</strong> {{film.opening_crawl}}</p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"film.title == ''\">\r\n        <div class=\"col\">\r\n            <p><strong>No information available</strong></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Characters</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"film.characters\">\r\n            <ng-container *ngIf=\"film.characters.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let characterItem of film.characters\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(characterItem.name, characterItem.url)\">{{characterItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"film.characters.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Planets</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"film.planets\">\r\n            <ng-container *ngIf=\"film.planets.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let planetsItem of film.planets\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(planetsItem.name, planetsItem.url)\">{{planetsItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n            <div class=\"col\" *ngIf=\"film.planets.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Starships</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"film.starships\">\r\n            <ng-container *ngIf=\"film.starships.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let starshipItem of film.starships\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(starshipItem.name, starshipItem.url)\">{{starshipItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"film.starships.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Vehicles</h5>\r\n        </div>\r\n\r\n        <ng-container *ngIf=\"film.vehicles\">\r\n            <ng-container *ngIf=\"film.vehicles.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let vehiclesItem of film.vehicles\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(vehiclesItem.name, vehiclesItem.url)\">{{vehiclesItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"film.vehicles.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Species</h5>\r\n        </div>\r\n\r\n        <ng-container *ngIf=\"film.species\">\r\n            <ng-container *ngIf=\"film.species.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let speciesItem of film.species\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(speciesItem.name, speciesItem.url)\">{{speciesItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"film.species.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/people/people.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/people/people.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreLayoutPeoplePeopleComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container px-0 px-sm-0 px-md-auto px-lg-auto px-xl-auto\" *ngIf=\"people\">\r\n    <div class=\"row\" *ngIf=\"people.name != ''\">\r\n        <div class=\"col-12 text-center text-sm-center text-md-left text-lg-left text-xl-left\">\r\n            <h5 class=\"mb-0\">{{people.name}}</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <hr *ngIf=\"people.name != ''\">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>General</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"people.name != ''\">\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Height:</strong> {{people.height | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Mass:</strong> {{people.mass | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Hair Color:</strong> {{people.hair_color | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Skin Color:</strong> {{people.skin_color | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Eyes Color:</strong> {{people.eye_color | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Birth Year:</strong> {{people.birth_year | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Gender:</strong> {{people.gender | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Homeworld:</strong> <button class=\"btn btn-link pt-0 pl-1\" type=\"button\" (click)=\"returnElementToHome(people.homeworld?.name, people.homeworld?.url)\">{{people.homeworld.name | titlecase}}</button> </p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"people.name == ''\">\r\n        <div class=\"col\">\r\n            <p><strong>No information available</strong></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Films</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"people.films\">\r\n            <ng-container *ngIf=\"people.films.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let film of people.films\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(film.title, film.url)\">{{film.title | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"people.films.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Species</h5>\r\n        </div>\r\n\r\n        <ng-container *ngIf=\"people.species\">\r\n            <ng-container *ngIf=\"people.species.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let especiesItem of people.species\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(especiesItem.name, especiesItem.url)\">{{especiesItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n            <div class=\"col\" *ngIf=\"people.species.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Vehicles</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"people.vehicles\">\r\n            <ng-container *ngIf=\"people.vehicles.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let vehicle of people.vehicles\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(vehicle.name, vehicle.url)\">{{vehicle.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"people.vehicles.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Spaceships</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"people.starships\">\r\n            <ng-container *ngIf=\"people.starships.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let spaceship of people.starships\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(spaceship.name, spaceship.url)\">{{spaceship.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"people.starships.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/planet/planet.component.html":
  /*!************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/planet/planet.component.html ***!
    \************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreLayoutPlanetPlanetComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container px-0 px-sm-0 px-md-auto px-lg-auto px-xl-auto\" *ngIf=\"planet\">\r\n    <div class=\"row\" *ngIf=\"planet.name != ''\">\r\n        <div class=\"col-12 text-center text-sm-center text-md-left text-lg-left text-xl-left\">\r\n            <h5 class=\"mb-0\">{{planet.name}}</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <hr *ngIf=\"planet.name != ''\">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>General</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"planet.name != ''\">\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Rotation Period:</strong> {{planet.rotation_period | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Orbital Period:</strong> {{planet.orbital_period | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Diameter:</strong> {{planet.diameter | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Climate:</strong> {{planet.climate | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Gravity:</strong> {{planet.gravity | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Terrain:</strong> {{planet.terrain | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Surface Water:</strong> {{planet.surface_water | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Population:</strong> {{planet.population | titlecase}}</p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"planet.name == ''\">\r\n        <div class=\"col\">\r\n            <p><strong>No information available</strong></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Residents</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"planet.residents\">\r\n            <ng-container *ngIf=\"planet.residents.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let resident of planet.residents\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(resident.name, resident.url)\">{{resident.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"planet.residents.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Films</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"planet.films\">\r\n            <ng-container *ngIf=\"planet.films.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let film of planet.films\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(film.title, film.url)\">{{film.title | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n            <div class=\"col\" *ngIf=\"planet.films.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/species/species.component.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/species/species.component.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreLayoutSpeciesSpeciesComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container px-0 px-sm-0 px-md-auto px-lg-auto px-xl-auto\" *ngIf=\"species\">\r\n    <div class=\"row\" *ngIf=\"species.name != ''\">\r\n        <div class=\"col-12 text-center text-sm-center text-md-left text-lg-left text-xl-left\">\r\n            <h5 class=\"mb-0\">{{species.name}}</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <hr *ngIf=\"species.name != ''\">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>General</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"species.name != ''\">\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Classification:</strong> {{species.classification | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Designation:</strong> {{species.designation | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Average Height:</strong> {{species.average_height | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Hair Color:</strong> {{species.hair_color | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Skin Color:</strong> {{species.skin_color | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Eyes Color:</strong> {{species.eye_color | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Average Lifespan:</strong> {{species.average_lifespan | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Language:</strong> {{species.language | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Homeworld:</strong> <button class=\"btn btn-link pt-0 pl-1\" type=\"button\" (click)=\"returnElementToHome(species.homeworld.name, species.homeworld.url)\">{{species.homeworld.name | titlecase}}</button></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"species.name == ''\">\r\n        <div class=\"col\">\r\n            <p><strong>No information available</strong></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>People</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"species.people\">\r\n            <ng-container *ngIf=\"species.people.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let peopleItem of species.people\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(peopleItem.name, peopleItem.url)\">{{peopleItem.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n            <div class=\"col\" *ngIf=\"species.people.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Films</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"species.films\">\r\n            <ng-container *ngIf=\"species.films.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let film of species.films\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(film.title, film.url)\">{{film.title | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"species.films.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/starship/starship.component.html":
  /*!****************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/starship/starship.component.html ***!
    \****************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreLayoutStarshipStarshipComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container px-0 px-sm-0 px-md-auto px-lg-auto px-xl-auto\" *ngIf=\"starship\">\r\n    <div class=\"row\" *ngIf=\"starship.name != ''\">\r\n        <div class=\"col-12 text-center text-sm-center text-md-left text-lg-left text-xl-left\">\r\n            <h5 class=\"mb-0\">{{starship.name}}</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <hr *ngIf=\"starship.name != ''\">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>General</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"starship.name != ''\">\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Model:</strong> {{starship.model | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Hyperdrive Rating:</strong> {{starship.hyperdrive_rating | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>MGLT:</strong> {{starship.mglt | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Consumables:</strong> {{starship.consumables | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Crew:</strong> {{starship.crew | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Length:</strong> {{starship.length | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Passengers:</strong> {{starship.passengers}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Max Atmosphering Speed:</strong> {{starship.max_atmosphering_speed | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Cost in Credits:</strong> {{starship.cost_in_credits | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Cargo Capacity:</strong> {{starship.cargo_capacity | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Starship Class:</strong> {{starship.starship_class | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Manufacturer:</strong> {{starship.manufacturer | titlecase}}</p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"starship.name == ''\">\r\n        <div class=\"col\">\r\n            <p><strong>No information available</strong></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Pilots</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"starship.pilots\">\r\n            <ng-container *ngIf=\"starship.pilots.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let pilot of starship.pilots\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(pilot.name, pilot.url)\">{{pilot.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n            <div class=\"col\" *ngIf=\"starship.pilots.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Films</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"starship.films\">\r\n            <ng-container *ngIf=\"starship.films.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let film of starship.films\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(film.title, film.url)\">{{film.title | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"starship.films.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/vehicle/vehicle.component.html":
  /*!**************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/vehicle/vehicle.component.html ***!
    \**************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppCoreLayoutVehicleVehicleComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container px-0 px-sm-0 px-md-auto px-lg-auto px-xl-auto\" *ngIf=\"vehicle\">\r\n    <div class=\"row\" *ngIf=\"vehicle.name != ''\">\r\n        <div class=\"col-12 text-center text-sm-center text-md-left text-lg-left text-xl-left\">\r\n            <h5 class=\"mb-0\">{{vehicle.name}}</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <hr *ngIf=\"vehicle.name != ''\">\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>General</h5>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"vehicle.name != ''\">\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Model:</strong> {{vehicle.model | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Crew:</strong> {{vehicle.crew | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Length:</strong> {{vehicle.length | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Passengers:</strong> {{vehicle.passengers}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Cargo Capacity:</strong> {{vehicle.cargo_capacity | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Consumables:</strong> {{vehicle.consumables | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Max Atmosphering Speed:</strong> {{vehicle.max_atmosphering_speed | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Cost in Credits:</strong> {{vehicle.cost_in_credits | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4\">\r\n            <p><strong>Vehicle Class:</strong> {{vehicle.vehicle_class | titlecase}}</p>\r\n        </div>\r\n        <div class=\"col-12 col-sm-12 col-md-6 col-lg-12 col-xl-12\">\r\n            <p><strong>Manufacturer:</strong> {{vehicle.manufacturer | titlecase}}</p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\" *ngIf=\"vehicle.name == ''\">\r\n        <div class=\"col\">\r\n            <p><strong>No information available</strong></p>\r\n        </div>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Pilots</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"vehicle.pilots\">\r\n            <ng-container *ngIf=\"vehicle.pilots.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let pilot of vehicle.pilots\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(pilot.name, pilot.url)\">{{pilot.name | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n            <div class=\"col\" *ngIf=\"vehicle.pilots.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n\r\n    <div class=\"row\">\r\n        <div class=\"col-12\">\r\n            <h5>Films</h5>\r\n        </div>\r\n        <ng-container *ngIf=\"vehicle.films\">\r\n            <ng-container *ngIf=\"vehicle.films.length > 0\">\r\n                <div class=\"col-6 col-sm-6 col-md-4 col-lg-4 col-xl-4\" *ngFor=\"let film of vehicle.films\">\r\n                    <button class=\"btn btn-link btn-block text-left\" type=\"button\" (click)=\"returnElementToHome(film.title, film.url)\">{{film.title | titlecase}}</button>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div class=\"col\" *ngIf=\"vehicle.films.length <= 0\">\r\n                <p><strong>No information available</strong></p>\r\n            </div>\r\n        </ng-container>\r\n    </div>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/main/home/home.component.html":
  /*!*************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/main/home/home.component.html ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppMainHomeHomeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container-fluid px-0\">\r\n    <header>\r\n        <div class=\"row black py-2\">\r\n            <div class=\"col-12 px-0 text-center\">\r\n                <img src=\"../../../assets/img/star-wars-logo.png\" alt=\"Star Wars Logo\">\r\n            </div>\r\n        </div>\r\n    </header>\r\n\r\n    <nav class=\"navbar-expand-sm navbar-expand-md navbar-expand-lg navbar-expand-xl bg-dark\">\r\n        <div class=\"text-right\">\r\n            <button class=\"btn btn-dark navbar-toggler collapsed my-1\" type=\"button\" id=\"btnCollapse\" data-toggle=\"collapse\" data-target=\"#navbarSupportedContent\" aria-controls=\"navbarSupportedContent\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\r\n                <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\r\n            </button>\r\n        </div>\r\n        <div class=\"row navbar-collapse collapse\" id=\"navbarSupportedContent\">\r\n            <div class=\"col-12 px-0\">\r\n                <div class=\"container px-0 px-sm-0 px-md-0 px-lg-auto px-xl-auto\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 py-1 nav-padding\" *ngFor=\"let category of lstCategories; let i=index\">\r\n                            <button type=\"button\" class=\"btn btn-block on-hover nav-link px-0 px-sm-0 px-md-0 px-lg-auto px-xl-auto\" [ngClass]=\"{'btn-link': i != idSelectedCategory, 'bg-white': i == idSelectedCategory}\" (click)=\"changeSelectedCategory(i)\">\r\n                                <label class=\"cursor-pointer\"\r\n                                    [ngClass]=\"{'text-white': i != idSelectedCategory, 'text.dark': i == idSelectedCategory}\">{{category.txcategorypage | titlecase}}</label>\r\n                            </button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </nav>\r\n\r\n    <main>\r\n        <div class=\"row\" *ngIf=\"lstCategories\">\r\n            <div class=\"col-12 px-0 pb-2\">\r\n                <div class=\"container mt-2 px-5 px-sm-0 px-md-0 px-lg-auto px-xl-auto\">\r\n                    <div class=\"row\">\r\n                        <div class=\"col-12\">\r\n                            <h2>{{lstCategories[idSelectedCategory].txcategorypage | titlecase}}</h2>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <hr>\r\n\r\n                    <div class=\"row\">\r\n                        <div class=\"col-12 col-sm-12 col-md-4 col-lg-3 col-xl-3\">\r\n                            <ng-container *ngIf=\"categoryPages\">\r\n                                <form #f (ngSubmit)=\"search()\">\r\n                                    <div class=\"row\">\r\n                                        <div class=\"col-9 pr-0\">\r\n                                            <div class=\"form-group\">\r\n                                                <div class=\"input-group\">\r\n                                                    <input type=\"text\" class=\"form-control\" placeholder=\"Search\" name=\"txtSearch\" [(ngModel)]=\"txtSearch\">\r\n                                                    <div class=\"input-group-append\">\r\n                                                        <button class=\"btn btn-outline-secondary\">\r\n                                                            <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\r\n                                                        </button>\r\n                                                    </div>\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n                                        <div class=\"col-3 pl-1\">\r\n                                            <div class=\"form-group\">\r\n                                                <button class=\"btn btn-dark form-control col-12 col-sm-12 col-md-0 col-lg-0 col-xl-0\" type=\"button\" id=\"btnCollapsePage\" data-toggle=\"collapse\" data-target=\"#collapseList\" aria-expanded=\"true\" aria-controls=\"collapseList\">\r\n                                                    <i class=\"fa fa-bars\" aria-hidden=\"true\"></i>\r\n                                                </button>\r\n                                            </div>\r\n                                        </div>\r\n                                    </div>\r\n                                </form>\r\n\r\n                                <ul class=\"list-group collapse show\" id=\"collapseList\">\r\n                                    <ng-container *ngIf=\"categoryPages.count > 0; else errorFound\">\r\n                                        <li class=\"list-group-item p-0 mt-1\" *ngFor=\"let resultItem of categoryPages.results; let i=index\">\r\n                                            <button type=\"button\" class=\"btn btn-block text-center\" [ngClass]=\"{'btn-link': resultItem.url != urlSelectedItem, 'btn-dark': resultItem.url == urlSelectedItem}\" (click)=\"displayItem(resultItem.url)\">\r\n                                                    <label class=\"cursor-pointer\" *ngIf=\"resultItem.title\">{{resultItem.title}}</label>\r\n                                                    <label class=\"cursor-pointer\" *ngIf=\"resultItem.name\">{{resultItem.name}}</label>\r\n                                                </button>\r\n                                        </li>\r\n\r\n                                        <li class=\"list-group-item p-0 mt-1 text-center\" id=\"pages\">\r\n                                            <div class=\"row\">\r\n                                                <div class=\"col-3\">\r\n                                                    <button type=\"button\" class=\"btn btn-link btn-block text-dark\" id=\"btnPrevious\" (click)=\"previousPage()\" *ngIf=\"countMaxPage > 1\" [disabled]=\"currentPage <= 0\">\r\n                                                    <i class=\"fas fa-arrow-left\" aria-hidden=\"true\"></i>\r\n                                                </button>\r\n                                                </div>\r\n                                                <div class=\"col-6\">\r\n                                                    <label class=\"pt-2\">\r\n                                                    Page {{currentPage + 1}} of {{countMaxPage}}\r\n                                                </label>\r\n                                                </div>\r\n                                                <div class=\"col-3\">\r\n                                                    <button type=\"button\" class=\"btn btn-link btn-block text-dark\" id=\"btnNext\" (click)=\"nextPage()\" *ngIf=\"countMaxPage > 1\" [disabled]=\"currentPage + 1 >= countMaxPage\">\r\n                                                    <i class=\"fas fa-arrow-right\"></i>\r\n                                                </button>\r\n                                                </div>\r\n                                            </div>\r\n                                        </li>\r\n                                    </ng-container>\r\n\r\n                                    <ng-template #errorFound>\r\n                                        <li class=\"list-group-item p-0 mt-1 text-center border-0\">\r\n                                            Nenhum resultado encontrado. Tente novamente!\r\n                                        </li>\r\n                                    </ng-template>\r\n                                </ul>\r\n\r\n                                <hr>\r\n                            </ng-container>\r\n                        </div>\r\n\r\n                        <div class=\"col-12 col-sm-12 col-md-8 col-lg-9 col-xl-9\">\r\n                            <ng-container *ngIf=\"idSelectedCategory == 0\">\r\n                                <app-film [urlSelectedItem]=\"urlSelectedItem\" (urlElementFromCategory)=\"showElementFromCategory($event)\"></app-film>\r\n                            </ng-container>\r\n\r\n                            <ng-container *ngIf=\"idSelectedCategory == 1\">\r\n                                <app-people [urlSelectedItem]=\"urlSelectedItem\" (urlElementFromCategory)=\"showElementFromCategory($event)\"></app-people>\r\n                            </ng-container>\r\n\r\n                            <ng-container *ngIf=\"idSelectedCategory == 2\">\r\n                                <app-planet [urlSelectedItem]=\"urlSelectedItem\" (urlElementFromCategory)=\"showElementFromCategory($event)\"></app-planet>\r\n                            </ng-container>\r\n\r\n                            <ng-container *ngIf=\"idSelectedCategory == 3\">\r\n                                <app-species [urlSelectedItem]=\"urlSelectedItem\" (urlElementFromCategory)=\"showElementFromCategory($event)\"></app-species>\r\n                            </ng-container>\r\n\r\n                            <ng-container *ngIf=\"idSelectedCategory == 4\">\r\n                                <app-starship [urlSelectedItem]=\"urlSelectedItem\" (urlElementFromCategory)=\"showElementFromCategory($event)\"></app-starship>\r\n                            </ng-container>\r\n\r\n                            <ng-container *ngIf=\"idSelectedCategory == 5\">\r\n                                <app-vehicle [urlSelectedItem]=\"urlSelectedItem\" (urlElementFromCategory)=\"showElementFromCategory($event)\"></app-vehicle>\r\n                            </ng-container>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div class=\"row\" *ngIf=\"showErrorMenssage\">\r\n            <div class=\"col-12 px-4\">\r\n                <p><strong>Error accessing site content. Reload the page and if the error persists, return again\r\n                        later.</strong></p>\r\n            </div>\r\n        </div>\r\n    </main>\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) {
          if (b.hasOwnProperty(p)) d[p] = b[p];
        }
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) {
            if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
          }
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];
      }

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
        if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      }
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) {
          try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];

            switch (op[0]) {
              case 0:
              case 1:
                t = op;
                break;

              case 4:
                _.label++;
                return {
                  value: op[1],
                  done: false
                };

              case 5:
                _.label++;
                y = op[1];
                op = [0];
                continue;

              case 7:
                op = _.ops.pop();

                _.trys.pop();

                continue;

              default:
                if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                  _ = 0;
                  continue;
                }

                if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                  _.label = op[1];
                  break;
                }

                if (op[0] === 6 && _.label < t[1]) {
                  _.label = t[1];
                  t = op;
                  break;
                }

                if (t && _.label < t[2]) {
                  _.label = t[2];

                  _.ops.push(op);

                  break;
                }

                if (t[2]) _.ops.pop();

                _.trys.pop();

                continue;
            }

            op = body.call(thisArg, _);
          } catch (e) {
            op = [6, e];
            y = 0;
          } finally {
            f = t = 0;
          }
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) {
        if (!exports.hasOwnProperty(p)) exports[p] = m[p];
      }
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) {
          ar.push(r.value);
        }
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) {
        ar = ar.concat(__read(arguments[i]));
      }

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) {
        s += arguments[i].length;
      }

      for (var r = Array(s), k = 0, i = 0; i < il; i++) {
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) {
          r[k] = a[j];
        }
      }

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) {
        if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      }
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _main_main_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./main/main-routing.module */
    "./src/app/main/main-routing.module.ts");

    var routes = [{
      path: "**",
      redirectTo: "home"
    }];

    var AppRoutingModule = function AppRoutingModule() {
      _classCallCheck(this, AppRoutingModule);
    };

    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_main_main_routing_module__WEBPACK_IMPORTED_MODULE_3__["MainRoutingModule"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.css":
  /*!***********************************!*\
    !*** ./src/app/app.component.css ***!
    \***********************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");

    var AppComponent = function AppComponent(ngxService) {
      _classCallCheck(this, AppComponent);

      this.ngxService = ngxService;
    };

    AppComponent.ctorParameters = function () {
      return [{
        type: ngx_ui_loader__WEBPACK_IMPORTED_MODULE_2__["NgxUiLoaderService"]
      }];
    };

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.css */
      "./src/app/app.component.css")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _main_main_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./main/main.module */
    "./src/app/main/main.module.ts");
    /* harmony import */


    var ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ngx-ui-loader */
    "./node_modules/ngx-ui-loader/fesm2015/ngx-ui-loader.js");

    var ngxUiLoaderConfig = {
      bgsColor: "red",
      bgsOpacity: 0.5,
      bgsPosition: "bottom-right",
      bgsSize: 60,
      bgsType: "ball-spin-clockwise",
      blur: 5,
      fgsColor: "white",
      fgsPosition: "center-center",
      fgsSize: 80,
      fgsType: "ball-scale-multiple",
      gap: 24,
      logoPosition: "center-center",
      logoSize: 240,
      logoUrl: "",
      masterLoaderId: "loader-01",
      overlayBorderRadius: "0",
      overlayColor: "rgba(40, 40, 40, 0.8)",
      pbColor: "red",
      pbDirection: "ltr",
      pbThickness: 3,
      hasProgressBar: true,
      text: "Buscando as informações. Aguarde!",
      textColor: "#FFFFFF",
      textPosition: "center-center"
    };

    var AppModule = function AppModule() {
      _classCallCheck(this, AppModule);
    };

    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"], _main_main_module__WEBPACK_IMPORTED_MODULE_6__["MainModule"], ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderModule"].forRoot(ngxUiLoaderConfig), ngx_ui_loader__WEBPACK_IMPORTED_MODULE_7__["NgxUiLoaderHttpModule"].forRoot({
        showForeground: true
      })],
      providers: [],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/core/core.module.ts":
  /*!*************************************!*\
    !*** ./src/app/core/core.module.ts ***!
    \*************************************/

  /*! exports provided: CoreModule */

  /***/
  function srcAppCoreCoreModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CoreModule", function () {
      return CoreModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _layout_people_people_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./layout/people/people.component */
    "./src/app/core/layout/people/people.component.ts");
    /* harmony import */


    var _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @ngx-loading-bar/http-client */
    "./node_modules/@ngx-loading-bar/http-client/fesm2015/ngx-loading-bar-http-client.js");
    /* harmony import */


    var _layout_film_film_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./layout/film/film.component */
    "./src/app/core/layout/film/film.component.ts");
    /* harmony import */


    var _layout_starship_starship_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./layout/starship/starship.component */
    "./src/app/core/layout/starship/starship.component.ts");
    /* harmony import */


    var _layout_planet_planet_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./layout/planet/planet.component */
    "./src/app/core/layout/planet/planet.component.ts");
    /* harmony import */


    var _layout_vehicle_vehicle_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./layout/vehicle/vehicle.component */
    "./src/app/core/layout/vehicle/vehicle.component.ts");
    /* harmony import */


    var _layout_species_species_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./layout/species/species.component */
    "./src/app/core/layout/species/species.component.ts");

    var CoreModule = function CoreModule() {
      _classCallCheck(this, CoreModule);
    };

    CoreModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_layout_people_people_component__WEBPACK_IMPORTED_MODULE_4__["PeopleComponent"], _layout_film_film_component__WEBPACK_IMPORTED_MODULE_6__["FilmComponent"], _layout_starship_starship_component__WEBPACK_IMPORTED_MODULE_7__["StarshipComponent"], _layout_planet_planet_component__WEBPACK_IMPORTED_MODULE_8__["PlanetComponent"], _layout_vehicle_vehicle_component__WEBPACK_IMPORTED_MODULE_9__["VehicleComponent"], _layout_species_species_component__WEBPACK_IMPORTED_MODULE_10__["SpeciesComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"], _ngx_loading_bar_http_client__WEBPACK_IMPORTED_MODULE_5__["LoadingBarHttpClientModule"]],
      exports: [_layout_people_people_component__WEBPACK_IMPORTED_MODULE_4__["PeopleComponent"], _layout_film_film_component__WEBPACK_IMPORTED_MODULE_6__["FilmComponent"], _layout_starship_starship_component__WEBPACK_IMPORTED_MODULE_7__["StarshipComponent"], _layout_planet_planet_component__WEBPACK_IMPORTED_MODULE_8__["PlanetComponent"], _layout_vehicle_vehicle_component__WEBPACK_IMPORTED_MODULE_9__["VehicleComponent"], _layout_species_species_component__WEBPACK_IMPORTED_MODULE_10__["SpeciesComponent"]]
    })], CoreModule);
    /***/
  },

  /***/
  "./src/app/core/layout/film/film.component.css":
  /*!*****************************************************!*\
    !*** ./src/app/core/layout/film/film.component.css ***!
    \*****************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreLayoutFilmFilmComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbGF5b3V0L2ZpbG0vZmlsbS5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/core/layout/film/film.component.ts":
  /*!****************************************************!*\
    !*** ./src/app/core/layout/film/film.component.ts ***!
    \****************************************************/

  /*! exports provided: FilmComponent */

  /***/
  function srcAppCoreLayoutFilmFilmComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FilmComponent", function () {
      return FilmComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _model_films_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../model/films.model */
    "./src/app/core/model/films.model.ts");
    /* harmony import */


    var _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_page_results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../model/page_results */
    "./src/app/core/model/page_results.ts");

    var FilmComponent =
    /*#__PURE__*/
    function () {
      /**
       * Injetando a dependência
       * @param swapiService
       */
      function FilmComponent(swapiService) {
        _classCallCheck(this, FilmComponent);

        this.swapiService = swapiService;
        this.urlElementFromCategory = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
       * Inicializando a variável
       */


      _createClass(FilmComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
          this.film = new _model_films_model__WEBPACK_IMPORTED_MODULE_2__["FilmsModule"]();

          if (this.urlSelectedItem.includes("films")) {
            this.checkFilm();
          }
        }
      }, {
        key: "ngOnChanges",
        value: function ngOnChanges() {
          if (this.film) {
            if (this.film.url != this.urlSelectedItem && this.urlSelectedItem.includes("films")) {
              this.checkFilm();
            }
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "checkFilm",
        value: function checkFilm() {
          var _this = this;

          this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(function (response) {
            _this.film = response[0]['getfilm'][0];

            if (typeof _this.film.characters[0] == "string") {
              _this.searchCharacters();
            }

            if (typeof _this.film.planets[0] == "string") {
              _this.searchPlanets();
            }

            if (typeof _this.film.species[0] == "string") {
              _this.searchSpecies();
            }

            if (typeof _this.film.starships[0] == "string") {
              _this.searchStarships();
            }

            if (typeof _this.film.vehicles[0] == "string") {
              _this.searchVehicles();
            }
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "searchCharacters",
        value: function searchCharacters() {
          var _this2 = this;

          this.film.characters.forEach(function (characterItem) {
            _this2.subscription = _this2.swapiService.getFromAPI(characterItem).subscribe(function (response) {
              _this2.film.characters[_this2.film.characters.indexOf(characterItem)] = response[0]['getpeople'][0];
            });
          });
        }
      }, {
        key: "searchPlanets",
        value: function searchPlanets() {
          var _this3 = this;

          this.film.planets.forEach(function (planetsItem) {
            _this3.subscription = _this3.swapiService.getFromAPI(planetsItem).subscribe(function (response) {
              _this3.film.planets[_this3.film.planets.indexOf(planetsItem)] = response[0]['getplanet'][0];
            });
          });
        }
      }, {
        key: "searchStarships",
        value: function searchStarships() {
          var _this4 = this;

          this.film.starships.forEach(function (starshipsItem) {
            _this4.subscription = _this4.swapiService.getFromAPI(starshipsItem).subscribe(function (response) {
              _this4.film.starships[_this4.film.starships.indexOf(starshipsItem)] = response[0]['getstarship'][0];
            });
          });
        }
      }, {
        key: "searchVehicles",
        value: function searchVehicles() {
          var _this5 = this;

          this.film.vehicles.forEach(function (vehiclesItem) {
            _this5.subscription = _this5.swapiService.getFromAPI(vehiclesItem).subscribe(function (response) {
              _this5.film.vehicles[_this5.film.vehicles.indexOf(vehiclesItem)] = response[0]['getvehicle'][0];
            });
          });
        }
      }, {
        key: "searchSpecies",
        value: function searchSpecies() {
          var _this6 = this;

          this.film.species.forEach(function (speciesItem) {
            _this6.subscription = _this6.swapiService.getFromAPI(speciesItem).subscribe(function (response) {
              _this6.film.species[_this6.film.species.indexOf(speciesItem)] = response[0]['getspecies'][0];
            });
          });
        }
      }, {
        key: "changeItem",
        value: function changeItem(item) {
          this.urlSelectedItem = item;
        }
      }, {
        key: "returnElementToHome",
        value: function returnElementToHome(elementName, elementUrl) {
          this.urlElementFromCategory.emit(new _model_page_results__WEBPACK_IMPORTED_MODULE_5__["PageResults"](elementName, elementUrl));
        }
      }]);

      return FilmComponent;
    }();

    FilmComponent.ctorParameters = function () {
      return [{
        type: _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__["SWAPIService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], FilmComponent.prototype, "urlSelectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], FilmComponent.prototype, "urlElementFromCategory", void 0);
    FilmComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-film',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./film.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/film/film.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./film.component.css */
      "./src/app/core/layout/film/film.component.css")).default]
    })], FilmComponent);
    /***/
  },

  /***/
  "./src/app/core/layout/people/people.component.css":
  /*!*********************************************************!*\
    !*** ./src/app/core/layout/people/people.component.css ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreLayoutPeoplePeopleComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbGF5b3V0L3Blb3BsZS9wZW9wbGUuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/core/layout/people/people.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/core/layout/people/people.component.ts ***!
    \********************************************************/

  /*! exports provided: PeopleComponent */

  /***/
  function srcAppCoreLayoutPeoplePeopleComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PeopleComponent", function () {
      return PeopleComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _model_people_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../model/people.model */
    "./src/app/core/model/people.model.ts");
    /* harmony import */


    var _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_page_results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../model/page_results */
    "./src/app/core/model/page_results.ts");

    var PeopleComponent =
    /*#__PURE__*/
    function () {
      /**
       * Injetando a dependência
       * @param swapiService
       */
      function PeopleComponent(swapiService) {
        _classCallCheck(this, PeopleComponent);

        this.swapiService = swapiService;
        this.urlElementFromCategory = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
       * Inicializando a variável
       */


      _createClass(PeopleComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
          this.people = new _model_people_model__WEBPACK_IMPORTED_MODULE_2__["PeopleModel"]();

          if (this.urlSelectedItem.includes('people')) {
            this.checkPeople();
          }
        }
        /**
         * Atualizando a variável
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges() {
          if (this.people) {
            if (this.people.url != this.urlSelectedItem && this.urlSelectedItem.includes('people')) {
              this.checkPeople();
            }
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "checkPeople",
        value: function checkPeople() {
          var _this7 = this;

          this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(function (response) {
            _this7.people = response[0]['getpeople'][0];

            if (typeof _this7.people.homeworld == "string") {
              _this7.searchHomeword();
            }

            if (typeof _this7.people.films[0] == "string") {
              _this7.searchFilms();
            }

            if (typeof _this7.people.species[0] == "string") {
              _this7.searchSpecies();
            }

            if (typeof _this7.people.starships[0] == "string") {
              _this7.searchStarships();
            }

            if (typeof _this7.people.vehicles[0] == "string") {
              _this7.searchVehicles();
            }
          });
        }
      }, {
        key: "searchHomeword",
        value: function searchHomeword() {
          var _this8 = this;

          var urlHomeword = typeof this.people.homeworld == "string" ? this.people.homeworld : this.people.homeworld.url;
          this.subscription = this.swapiService.getFromAPI(urlHomeword).subscribe(function (response) {
            _this8.people.homeworld = response[0]['getplanet'][0];
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "searchFilms",
        value: function searchFilms() {
          var _this9 = this;

          this.people.films.forEach(function (filmsItem) {
            _this9.subscription = _this9.swapiService.getFromAPI(filmsItem).subscribe(function (response) {
              _this9.people.films[_this9.people.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
            });
          });
        }
      }, {
        key: "searchStarships",
        value: function searchStarships() {
          var _this10 = this;

          this.people.starships.forEach(function (starshipsItem) {
            _this10.subscription = _this10.swapiService.getFromAPI(starshipsItem).subscribe(function (response) {
              _this10.people.starships[_this10.people.starships.indexOf(starshipsItem)] = response[0]['getstarship'][0];
            });
          });
        }
      }, {
        key: "searchVehicles",
        value: function searchVehicles() {
          var _this11 = this;

          this.people.vehicles.forEach(function (vehiclesItem) {
            _this11.subscription = _this11.swapiService.getFromAPI(vehiclesItem).subscribe(function (response) {
              _this11.people.vehicles[_this11.people.vehicles.indexOf(vehiclesItem)] = response[0]['getvehicle'][0];
            });
          });
        }
      }, {
        key: "searchSpecies",
        value: function searchSpecies() {
          var _this12 = this;

          this.people.species.forEach(function (speciesItem) {
            _this12.subscription = _this12.swapiService.getFromAPI(speciesItem).subscribe(function (response) {
              _this12.people.species[_this12.people.species.indexOf(speciesItem)] = response[0]['getspecies'][0];
            });
          });
        }
      }, {
        key: "returnElementToHome",
        value: function returnElementToHome(elementName, elementUrl) {
          this.urlElementFromCategory.emit(new _model_page_results__WEBPACK_IMPORTED_MODULE_5__["PageResults"](elementName, elementUrl));
        }
      }]);

      return PeopleComponent;
    }();

    PeopleComponent.ctorParameters = function () {
      return [{
        type: _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__["SWAPIService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], PeopleComponent.prototype, "urlSelectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PeopleComponent.prototype, "urlElementFromCategory", void 0);
    PeopleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-people',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./people.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/people/people.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./people.component.css */
      "./src/app/core/layout/people/people.component.css")).default]
    })], PeopleComponent);
    /***/
  },

  /***/
  "./src/app/core/layout/planet/planet.component.css":
  /*!*********************************************************!*\
    !*** ./src/app/core/layout/planet/planet.component.css ***!
    \*********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreLayoutPlanetPlanetComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbGF5b3V0L3BsYW5ldC9wbGFuZXQuY29tcG9uZW50LmNzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/core/layout/planet/planet.component.ts":
  /*!********************************************************!*\
    !*** ./src/app/core/layout/planet/planet.component.ts ***!
    \********************************************************/

  /*! exports provided: PlanetComponent */

  /***/
  function srcAppCoreLayoutPlanetPlanetComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlanetComponent", function () {
      return PlanetComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _model_planets_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../model/planets.model */
    "./src/app/core/model/planets.model.ts");
    /* harmony import */


    var _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_page_results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../model/page_results */
    "./src/app/core/model/page_results.ts");

    var PlanetComponent =
    /*#__PURE__*/
    function () {
      /**
       * Injetando a dependência
       * @param swapiService
       */
      function PlanetComponent(swapiService) {
        _classCallCheck(this, PlanetComponent);

        this.swapiService = swapiService;
        this.urlElementFromCategory = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
       * Inicializando a variável
       */


      _createClass(PlanetComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
          this.planet = new _model_planets_model__WEBPACK_IMPORTED_MODULE_2__["PlanetsModel"]();

          if (this.urlSelectedItem.includes("planets")) {
            this.checkPlanet();
          }
        }
        /**
         * Atualizando a variável
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges() {
          if (this.planet) {
            if (this.planet.url != this.urlSelectedItem && this.urlSelectedItem.includes("planets")) {
              this.checkPlanet();
            }
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "checkPlanet",
        value: function checkPlanet() {
          var _this13 = this;

          this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(function (response) {
            _this13.planet = response[0]['getplanet'][0];

            if (typeof _this13.planet.residents[0] == "string") {
              _this13.searchResidents();
            }

            if (typeof _this13.planet.films[0] == "string") {
              _this13.searchFilms();
            }
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "searchFilms",
        value: function searchFilms() {
          var _this14 = this;

          this.planet.films.forEach(function (filmsItem) {
            _this14.subscription = _this14.swapiService.getFromAPI(filmsItem).subscribe(function (response) {
              _this14.planet.films[_this14.planet.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
            });
          });
        }
      }, {
        key: "searchResidents",
        value: function searchResidents() {
          var _this15 = this;

          this.planet.residents.forEach(function (residentItem) {
            _this15.subscription = _this15.swapiService.getFromAPI(residentItem).subscribe(function (response) {
              _this15.planet.residents[_this15.planet.residents.indexOf(residentItem)] = response[0]['getpeople'][0];
            });
          });
        }
      }, {
        key: "returnElementToHome",
        value: function returnElementToHome(elementName, elementUrl) {
          this.urlElementFromCategory.emit(new _model_page_results__WEBPACK_IMPORTED_MODULE_5__["PageResults"](elementName, elementUrl));
        }
      }]);

      return PlanetComponent;
    }();

    PlanetComponent.ctorParameters = function () {
      return [{
        type: _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__["SWAPIService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], PlanetComponent.prototype, "urlSelectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], PlanetComponent.prototype, "urlElementFromCategory", void 0);
    PlanetComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-planet',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./planet.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/planet/planet.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./planet.component.css */
      "./src/app/core/layout/planet/planet.component.css")).default]
    })], PlanetComponent);
    /***/
  },

  /***/
  "./src/app/core/layout/species/species.component.css":
  /*!***********************************************************!*\
    !*** ./src/app/core/layout/species/species.component.css ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreLayoutSpeciesSpeciesComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbGF5b3V0L3NwZWNpZXMvc3BlY2llcy5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/core/layout/species/species.component.ts":
  /*!**********************************************************!*\
    !*** ./src/app/core/layout/species/species.component.ts ***!
    \**********************************************************/

  /*! exports provided: SpeciesComponent */

  /***/
  function srcAppCoreLayoutSpeciesSpeciesComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SpeciesComponent", function () {
      return SpeciesComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _model_species_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../model/species.model */
    "./src/app/core/model/species.model.ts");
    /* harmony import */


    var _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_page_results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../model/page_results */
    "./src/app/core/model/page_results.ts");

    var SpeciesComponent =
    /*#__PURE__*/
    function () {
      /**
       * Injetando a dependência
       * @param swapiService
       */
      function SpeciesComponent(swapiService) {
        _classCallCheck(this, SpeciesComponent);

        this.swapiService = swapiService;
        this.urlElementFromCategory = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
       * Inicializando a variável
       */


      _createClass(SpeciesComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
          this.species = new _model_species_model__WEBPACK_IMPORTED_MODULE_2__["SpeciesModel"]();

          if (this.urlSelectedItem.includes("species")) {
            this.checkSpecies();
          }
        }
        /**
         * Atualizando a variável
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges() {
          if (this.species) {
            if (this.species.url != this.urlSelectedItem && this.urlSelectedItem.includes("species")) {
              this.checkSpecies();
            }
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "checkSpecies",
        value: function checkSpecies() {
          var _this16 = this;

          this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(function (response) {
            _this16.species = response[0]['getspecies'][0];

            if (typeof _this16.species.homeworld[0] == "string") {
              _this16.searchHomeword();
            }

            if (typeof _this16.species.people[0] == "string") {
              _this16.searchPeople();
            }

            if (typeof _this16.species.films[0] == "string") {
              _this16.searchFilms();
            }
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "searchHomeword",
        value: function searchHomeword() {
          var _this17 = this;

          var urlHomeWord = this.species.homeworld[0];
          this.subscription = this.swapiService.getFromAPI(urlHomeWord).subscribe(function (response) {
            _this17.species.homeworld = response[0]['getplanet'][0];
          });
        }
      }, {
        key: "searchFilms",
        value: function searchFilms() {
          var _this18 = this;

          this.species.films.forEach(function (filmsItem) {
            _this18.subscription = _this18.swapiService.getFromAPI(filmsItem).subscribe(function (response) {
              _this18.species.films[_this18.species.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
            });
          });
        }
      }, {
        key: "searchPeople",
        value: function searchPeople() {
          var _this19 = this;

          this.species.people.forEach(function (peopleItem) {
            _this19.subscription = _this19.swapiService.getFromAPI(peopleItem).subscribe(function (response) {
              _this19.species.people[_this19.species.people.indexOf(peopleItem)] = response[0]['getpeople'][0];
            });
          });
        }
      }, {
        key: "returnElementToHome",
        value: function returnElementToHome(elementName, elementUrl) {
          this.urlElementFromCategory.emit(new _model_page_results__WEBPACK_IMPORTED_MODULE_5__["PageResults"](elementName, elementUrl));
        }
      }]);

      return SpeciesComponent;
    }();

    SpeciesComponent.ctorParameters = function () {
      return [{
        type: _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__["SWAPIService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], SpeciesComponent.prototype, "urlSelectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], SpeciesComponent.prototype, "urlElementFromCategory", void 0);
    SpeciesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-species',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./species.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/species/species.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./species.component.css */
      "./src/app/core/layout/species/species.component.css")).default]
    })], SpeciesComponent);
    /***/
  },

  /***/
  "./src/app/core/layout/starship/starship.component.css":
  /*!*************************************************************!*\
    !*** ./src/app/core/layout/starship/starship.component.css ***!
    \*************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreLayoutStarshipStarshipComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbGF5b3V0L3N0YXJzaGlwL3N0YXJzaGlwLmNvbXBvbmVudC5jc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/core/layout/starship/starship.component.ts":
  /*!************************************************************!*\
    !*** ./src/app/core/layout/starship/starship.component.ts ***!
    \************************************************************/

  /*! exports provided: StarshipComponent */

  /***/
  function srcAppCoreLayoutStarshipStarshipComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StarshipComponent", function () {
      return StarshipComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _model_starship_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../model/starship.model */
    "./src/app/core/model/starship.model.ts");
    /* harmony import */


    var _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_page_results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../model/page_results */
    "./src/app/core/model/page_results.ts");

    var StarshipComponent =
    /*#__PURE__*/
    function () {
      /**
        * Injetando a dependência
        * @param swapiServices
       */
      function StarshipComponent(swapiService) {
        _classCallCheck(this, StarshipComponent);

        this.swapiService = swapiService;
        this.urlElementFromCategory = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
        * Inicializando a variável
        */


      _createClass(StarshipComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
          this.starship = new _model_starship_model__WEBPACK_IMPORTED_MODULE_2__["StarshipModel"]();

          if (this.urlSelectedItem.includes("starships")) {
            this.checkStarship();
          }
        }
        /**
          * Atualizando a variável
          */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges() {
          if (this.starship) {
            if (this.starship.url != this.urlSelectedItem && this.urlSelectedItem.includes("starships")) {
              this.checkStarship();
            }
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "checkStarship",
        value: function checkStarship() {
          var _this20 = this;

          this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(function (response) {
            _this20.starship = response[0]['getstarship'][0];

            if (typeof _this20.starship.pilots[0] == "string") {
              _this20.searchPilots();
            }

            if (typeof _this20.starship.films[0] == "string") {
              _this20.searchFilms();
            }
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "searchFilms",
        value: function searchFilms() {
          var _this21 = this;

          this.starship.films.forEach(function (filmsItem) {
            _this21.subscription = _this21.swapiService.getFromAPI(filmsItem).subscribe(function (response) {
              _this21.starship.films[_this21.starship.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
            });
          });
        }
      }, {
        key: "searchPilots",
        value: function searchPilots() {
          var _this22 = this;

          this.starship.pilots.forEach(function (pilotsItem) {
            _this22.subscription = _this22.swapiService.getFromAPI(pilotsItem).subscribe(function (response) {
              _this22.starship.pilots[_this22.starship.pilots.indexOf(pilotsItem)] = response[0]['getpeople'][0];
            });
          });
        }
      }, {
        key: "returnElementToHome",
        value: function returnElementToHome(elementName, elementUrl) {
          this.urlElementFromCategory.emit(new _model_page_results__WEBPACK_IMPORTED_MODULE_5__["PageResults"](elementName, elementUrl));
        }
      }]);

      return StarshipComponent;
    }();

    StarshipComponent.ctorParameters = function () {
      return [{
        type: _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__["SWAPIService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], StarshipComponent.prototype, "urlSelectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], StarshipComponent.prototype, "urlElementFromCategory", void 0);
    StarshipComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-starship',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./starship.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/starship/starship.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./starship.component.css */
      "./src/app/core/layout/starship/starship.component.css")).default]
    })], StarshipComponent);
    /***/
  },

  /***/
  "./src/app/core/layout/vehicle/vehicle.component.css":
  /*!***********************************************************!*\
    !*** ./src/app/core/layout/vehicle/vehicle.component.css ***!
    \***********************************************************/

  /*! exports provided: default */

  /***/
  function srcAppCoreLayoutVehicleVehicleComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvcmUvbGF5b3V0L3ZlaGljbGUvdmVoaWNsZS5jb21wb25lbnQuY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/core/layout/vehicle/vehicle.component.ts":
  /*!**********************************************************!*\
    !*** ./src/app/core/layout/vehicle/vehicle.component.ts ***!
    \**********************************************************/

  /*! exports provided: VehicleComponent */

  /***/
  function srcAppCoreLayoutVehicleVehicleComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VehicleComponent", function () {
      return VehicleComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _model_vehicles_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../model/vehicles.model */
    "./src/app/core/model/vehicles.model.ts");
    /* harmony import */


    var _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _model_page_results__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../model/page_results */
    "./src/app/core/model/page_results.ts");

    var VehicleComponent =
    /*#__PURE__*/
    function () {
      /**
       * Injetando a dependência
       * @param swapiServices
       */
      function VehicleComponent(swapiService) {
        _classCallCheck(this, VehicleComponent);

        this.swapiService = swapiService;
        this.urlElementFromCategory = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
      }
      /**
       * Inicializando a variável
       */


      _createClass(VehicleComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          this.subscription = new rxjs__WEBPACK_IMPORTED_MODULE_4__["Subscription"]();
          this.vehicle = new _model_vehicles_model__WEBPACK_IMPORTED_MODULE_2__["VehiclesModel"]();

          if (this.urlSelectedItem.includes("vehicles")) {
            this.checkVehicles();
          }
        }
        /**
         * Atualizando a variável
         */

      }, {
        key: "ngOnChanges",
        value: function ngOnChanges() {
          if (this.vehicle) {
            if (this.vehicle.url != this.urlSelectedItem && this.urlSelectedItem.includes("vehicles")) {
              this.checkVehicles();
            }
          }
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
      }, {
        key: "checkVehicles",
        value: function checkVehicles() {
          var _this23 = this;

          this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(function (response) {
            _this23.vehicle = response[0]['getvehicle'][0];

            if (typeof _this23.vehicle.pilots[0] == "string") {
              _this23.searchPilots();
            }

            if (typeof _this23.vehicle.films[0] == "string") {
              _this23.searchFilms();
            }
          }, function (error) {
            console.log(error);
          });
        }
      }, {
        key: "searchFilms",
        value: function searchFilms() {
          var _this24 = this;

          this.vehicle.films.forEach(function (filmsItem) {
            _this24.subscription = _this24.swapiService.getFromAPI(filmsItem).subscribe(function (response) {
              _this24.vehicle.films[_this24.vehicle.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
            }, function (error) {
              console.log(error);
            });
          });
        }
      }, {
        key: "searchPilots",
        value: function searchPilots() {
          var _this25 = this;

          this.vehicle.pilots.forEach(function (pilotsItem) {
            _this25.subscription = _this25.swapiService.getFromAPI(pilotsItem).subscribe(function (response) {
              _this25.vehicle.pilots[_this25.vehicle.pilots.indexOf(pilotsItem)] = response[0]['getpeople'][0];
            }, function (error) {
              console.log(error);
            });
          });
        }
      }, {
        key: "returnElementToHome",
        value: function returnElementToHome(elementName, elementUrl) {
          this.urlElementFromCategory.emit(new _model_page_results__WEBPACK_IMPORTED_MODULE_5__["PageResults"](elementName, elementUrl));
        }
      }]);

      return VehicleComponent;
    }();

    VehicleComponent.ctorParameters = function () {
      return [{
        type: _services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_3__["SWAPIService"]
      }];
    };

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], VehicleComponent.prototype, "urlSelectedItem", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])()], VehicleComponent.prototype, "urlElementFromCategory", void 0);
    VehicleComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-vehicle',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./vehicle.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/core/layout/vehicle/vehicle.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./vehicle.component.css */
      "./src/app/core/layout/vehicle/vehicle.component.css")).default]
    })], VehicleComponent);
    /***/
  },

  /***/
  "./src/app/core/model/categories.model.ts":
  /*!************************************************!*\
    !*** ./src/app/core/model/categories.model.ts ***!
    \************************************************/

  /*! exports provided: CategoriesModel */

  /***/
  function srcAppCoreModelCategoriesModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CategoriesModel", function () {
      return CategoriesModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var CategoriesModel =
    /*#__PURE__*/
    function () {
      function CategoriesModel() {
        var txcategorypage = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
        var idCategorypage = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

        _classCallCheck(this, CategoriesModel);

        this.txcategorypage = txcategorypage;
        this.idCategorypage = idCategorypage;
      }

      _createClass(CategoriesModel, [{
        key: "getIdCategorypage",
        value: function getIdCategorypage() {
          return this.idCategorypage;
        }
      }]);

      return CategoriesModel;
    }();
    /***/

  },

  /***/
  "./src/app/core/model/films.model.ts":
  /*!*******************************************!*\
    !*** ./src/app/core/model/films.model.ts ***!
    \*******************************************/

  /*! exports provided: FilmsModule */

  /***/
  function srcAppCoreModelFilmsModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FilmsModule", function () {
      return FilmsModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var FilmsModule = function FilmsModule() {
      var title = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var episode_id = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var opening_crawl = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      var director = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
      var producer = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
      var release_date = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
      var characters = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : [];
      var planets = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : [];
      var starships = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : [];
      var vehicles = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : [];
      var species = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : [];
      var created = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : "";
      var edited = arguments.length > 12 && arguments[12] !== undefined ? arguments[12] : "";
      var url = arguments.length > 13 && arguments[13] !== undefined ? arguments[13] : "";

      _classCallCheck(this, FilmsModule);

      this.title = title;
      this.episode_id = episode_id;
      this.opening_crawl = opening_crawl;
      this.director = director;
      this.producer = producer;
      this.release_date = release_date;
      this.characters = characters;
      this.planets = planets;
      this.starships = starships;
      this.vehicles = vehicles;
      this.species = species;
      this.created = created;
      this.edited = edited;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/model/page.model.ts":
  /*!******************************************!*\
    !*** ./src/app/core/model/page.model.ts ***!
    \******************************************/

  /*! exports provided: PageModel */

  /***/
  function srcAppCoreModelPageModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PageModel", function () {
      return PageModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var PageModel = function PageModel() {
      var count = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
      var next = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var previous = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
      var results = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : [];

      _classCallCheck(this, PageModel);

      this.count = count;
      this.next = next;
      this.previous = previous;
      this.results = results;
    };
    /***/

  },

  /***/
  "./src/app/core/model/page_results.ts":
  /*!********************************************!*\
    !*** ./src/app/core/model/page_results.ts ***!
    \********************************************/

  /*! exports provided: PageResults */

  /***/
  function srcAppCoreModelPage_resultsTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PageResults", function () {
      return PageResults;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var PageResults = function PageResults() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';
      var url = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

      _classCallCheck(this, PageResults);

      this.name = name;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/model/people.model.ts":
  /*!********************************************!*\
    !*** ./src/app/core/model/people.model.ts ***!
    \********************************************/

  /*! exports provided: PeopleModel */

  /***/
  function srcAppCoreModelPeopleModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PeopleModel", function () {
      return PeopleModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var PeopleModel = function PeopleModel() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var height = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var mass = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      var hair_color = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
      var skin_color = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
      var eye_color = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
      var birth_year = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : "";
      var gender = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : "";
      var homeworld = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : "";
      var films = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : [];
      var species = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : [];
      var vehicles = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : [];
      var starships = arguments.length > 12 && arguments[12] !== undefined ? arguments[12] : [];
      var created = arguments.length > 13 && arguments[13] !== undefined ? arguments[13] : "";
      var edited = arguments.length > 14 && arguments[14] !== undefined ? arguments[14] : "";
      var url = arguments.length > 15 && arguments[15] !== undefined ? arguments[15] : "";

      _classCallCheck(this, PeopleModel);

      this.name = name;
      this.height = height;
      this.mass = mass;
      this.hair_color = hair_color;
      this.skin_color = skin_color;
      this.eye_color = eye_color;
      this.birth_year = birth_year;
      this.gender = gender;
      this.homeworld = homeworld;
      this.films = films;
      this.species = species;
      this.vehicles = vehicles;
      this.starships = starships;
      this.created = created;
      this.edited = edited;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/model/planets.model.ts":
  /*!*********************************************!*\
    !*** ./src/app/core/model/planets.model.ts ***!
    \*********************************************/

  /*! exports provided: PlanetsModel */

  /***/
  function srcAppCoreModelPlanetsModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PlanetsModel", function () {
      return PlanetsModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var PlanetsModel = function PlanetsModel() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var rotation_period = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var orbital_period = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      var diameter = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
      var climate = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
      var gravity = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
      var terrain = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : "";
      var surface_water = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : "";
      var population = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : "";
      var residents = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : [];
      var films = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : [];
      var created = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : "";
      var edited = arguments.length > 12 && arguments[12] !== undefined ? arguments[12] : "";
      var url = arguments.length > 13 && arguments[13] !== undefined ? arguments[13] : "";

      _classCallCheck(this, PlanetsModel);

      this.name = name;
      this.rotation_period = rotation_period;
      this.orbital_period = orbital_period;
      this.diameter = diameter;
      this.climate = climate;
      this.gravity = gravity;
      this.terrain = terrain;
      this.surface_water = surface_water;
      this.population = population;
      this.residents = residents;
      this.films = films;
      this.created = created;
      this.edited = edited;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/model/species.model.ts":
  /*!*********************************************!*\
    !*** ./src/app/core/model/species.model.ts ***!
    \*********************************************/

  /*! exports provided: SpeciesModel */

  /***/
  function srcAppCoreModelSpeciesModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SpeciesModel", function () {
      return SpeciesModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var SpeciesModel = function SpeciesModel() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var classification = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var designation = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      var average_height = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
      var skin_color = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
      var hair_color = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
      var eye_color = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : "";
      var average_lifespan = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : "";
      var homeworld = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : "";
      var language = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : "";
      var people = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : [];
      var films = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : [];
      var created = arguments.length > 12 && arguments[12] !== undefined ? arguments[12] : "";
      var edited = arguments.length > 13 && arguments[13] !== undefined ? arguments[13] : "";
      var url = arguments.length > 14 && arguments[14] !== undefined ? arguments[14] : "";

      _classCallCheck(this, SpeciesModel);

      this.name = name;
      this.classification = classification;
      this.designation = designation;
      this.average_height = average_height;
      this.skin_color = skin_color;
      this.hair_color = hair_color;
      this.eye_color = eye_color;
      this.average_lifespan = average_lifespan;
      this.homeworld = homeworld;
      this.language = language;
      this.people = people;
      this.films = films;
      this.created = created;
      this.edited = edited;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/model/starship.model.ts":
  /*!**********************************************!*\
    !*** ./src/app/core/model/starship.model.ts ***!
    \**********************************************/

  /*! exports provided: StarshipModel */

  /***/
  function srcAppCoreModelStarshipModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "StarshipModel", function () {
      return StarshipModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var StarshipModel = function StarshipModel() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var model = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var manufacturer = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      var cost_in_credits = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
      var length = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
      var max_atmosphering_speed = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
      var crew = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : "";
      var passengers = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : "";
      var cargo_capacity = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : "";
      var consumables = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : "";
      var hyperdrive_rating = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : "";
      var mglt = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : "";
      var starship_class = arguments.length > 12 && arguments[12] !== undefined ? arguments[12] : "";
      var pilots = arguments.length > 13 && arguments[13] !== undefined ? arguments[13] : [];
      var films = arguments.length > 14 && arguments[14] !== undefined ? arguments[14] : [];
      var created = arguments.length > 15 && arguments[15] !== undefined ? arguments[15] : "";
      var edited = arguments.length > 16 && arguments[16] !== undefined ? arguments[16] : "";
      var url = arguments.length > 17 && arguments[17] !== undefined ? arguments[17] : "";

      _classCallCheck(this, StarshipModel);

      this.name = name;
      this.model = model;
      this.manufacturer = manufacturer;
      this.cost_in_credits = cost_in_credits;
      this.length = length;
      this.max_atmosphering_speed = max_atmosphering_speed;
      this.crew = crew;
      this.passengers = passengers;
      this.cargo_capacity = cargo_capacity;
      this.consumables = consumables;
      this.hyperdrive_rating = hyperdrive_rating;
      this.mglt = mglt;
      this.starship_class = starship_class;
      this.pilots = pilots;
      this.films = films;
      this.created = created;
      this.edited = edited;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/model/vehicles.model.ts":
  /*!**********************************************!*\
    !*** ./src/app/core/model/vehicles.model.ts ***!
    \**********************************************/

  /*! exports provided: VehiclesModel */

  /***/
  function srcAppCoreModelVehiclesModelTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "VehiclesModel", function () {
      return VehiclesModel;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var VehiclesModel = function VehiclesModel() {
      var name = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : "";
      var model = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
      var manufacturer = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : "";
      var cost_in_credits = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : "";
      var length = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
      var max_atmosphering_speed = arguments.length > 5 && arguments[5] !== undefined ? arguments[5] : "";
      var crew = arguments.length > 6 && arguments[6] !== undefined ? arguments[6] : "";
      var passengers = arguments.length > 7 && arguments[7] !== undefined ? arguments[7] : "";
      var cargo_capacity = arguments.length > 8 && arguments[8] !== undefined ? arguments[8] : "";
      var consumables = arguments.length > 9 && arguments[9] !== undefined ? arguments[9] : "";
      var vehicle_class = arguments.length > 10 && arguments[10] !== undefined ? arguments[10] : "";
      var pilots = arguments.length > 11 && arguments[11] !== undefined ? arguments[11] : [];
      var films = arguments.length > 12 && arguments[12] !== undefined ? arguments[12] : [];
      var created = arguments.length > 13 && arguments[13] !== undefined ? arguments[13] : "";
      var edited = arguments.length > 14 && arguments[14] !== undefined ? arguments[14] : "";
      var url = arguments.length > 15 && arguments[15] !== undefined ? arguments[15] : "";

      _classCallCheck(this, VehiclesModel);

      this.name = name;
      this.model = model;
      this.manufacturer = manufacturer;
      this.cost_in_credits = cost_in_credits;
      this.length = length;
      this.max_atmosphering_speed = max_atmosphering_speed;
      this.crew = crew;
      this.passengers = passengers;
      this.cargo_capacity = cargo_capacity;
      this.consumables = consumables;
      this.vehicle_class = vehicle_class;
      this.pilots = pilots;
      this.films = films;
      this.created = created;
      this.edited = edited;
      this.url = url;
    };
    /***/

  },

  /***/
  "./src/app/core/services/events/events.service.ts":
  /*!********************************************************!*\
    !*** ./src/app/core/services/events/events.service.ts ***!
    \********************************************************/

  /*! exports provided: EventsService */

  /***/
  function srcAppCoreServicesEventsEventsServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "EventsService", function () {
      return EventsService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var util__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! util */
    "./node_modules/util/util.js");
    /* harmony import */


    var util__WEBPACK_IMPORTED_MODULE_2___default =
    /*#__PURE__*/
    __webpack_require__.n(util__WEBPACK_IMPORTED_MODULE_2__);

    var EventsService =
    /*#__PURE__*/
    function () {
      /**
       * Inicializando a variável
       */
      function EventsService() {
        _classCallCheck(this, EventsService);

        this.events = [];
      }
      /**
       * Método responsável por adicionar um novo evento à lista
       * @param tempEvent
       */


      _createClass(EventsService, [{
        key: "addEvent",
        value: function addEvent(tempEvent) {
          if (this.eventExists(tempEvent.log)) {
            this.removeEvent(tempEvent.log);
          }

          this.events.push(tempEvent);
        }
        /**
         * Método responsável por verificar a existencia de um evento na lista
         * @param log
         */

      }, {
        key: "eventExists",
        value: function eventExists(log) {
          var tempEvent = this.events.filter(function (value, index, arr) {
            return value.log == log;
          });
          return tempEvent[0] ? true : false;
        }
        /**
         * Método responsável por procurar um evento na lista e retornar seu id
         * @param log
         */

      }, {
        key: "returnEventId",
        value: function returnEventId(log) {
          var tempEvent = this.events.filter(function (value, index, arr) {
            return value.log == log;
          });
          return Object(util__WEBPACK_IMPORTED_MODULE_2__["isUndefined"])(tempEvent[0]) ? -1 : tempEvent[0].id;
        }
        /**
         * Método responsavel por remover evento da lista
         * @param log
         */

      }, {
        key: "removeEvent",
        value: function removeEvent(log) {
          this.events = this.events.filter(function (value, index, arr) {
            return value.log != log;
          });
        }
      }]);

      return EventsService;
    }();

    EventsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], EventsService);
    /***/
  },

  /***/
  "./src/app/core/services/swapi/swapi.service.ts":
  /*!******************************************************!*\
    !*** ./src/app/core/services/swapi/swapi.service.ts ***!
    \******************************************************/

  /*! exports provided: SWAPIService */

  /***/
  function srcAppCoreServicesSwapiSwapiServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SWAPIService", function () {
      return SWAPIService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var rxjs_operators__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! rxjs/operators */
    "./node_modules/rxjs/_esm2015/operators/index.js");

    var SWAPIService =
    /*#__PURE__*/
    function () {
      /**
       * Injeta as dependência, inicializa as variáveis e chama o método 'searchCategories'
       * @param httpClient
       */
      function SWAPIService(httpClient) {
        _classCallCheck(this, SWAPIService);

        this.httpClient = httpClient;
      }
      /**
       * Método responsável por retornar o get de uma requisição http
       */


      _createClass(SWAPIService, [{
        key: "getFromAPI",
        value: function getFromAPI(url) {
          return this.httpClient.get(url).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["retry"])(3), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_4__["catchError"])(this.handleError));
        }
      }, {
        key: "handleError",
        value: function handleError(error) {
          if (error.error instanceof ErrorEvent) {
            // A client-side or network error occurred. Handle it accordingly.
            console.error('An error occurred:', error.error.message);
          } else {
            // The backend returned an unsuccessful response code.
            // The response body may contain clues as to what went wrong,
            console.error("Backend returned code ".concat(error.status, ", ") + "body was: ".concat(error.error));
          } // return an observable with a user-facing error message


          return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])('Something bad happened; please try again later.');
        }
      }]);

      return SWAPIService;
    }();

    SWAPIService.ctorParameters = function () {
      return [{
        type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
      }];
    };

    SWAPIService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], SWAPIService);
    /***/
  },

  /***/
  "./src/app/main/home/home.component.css":
  /*!**********************************************!*\
    !*** ./src/app/main/home/home.component.css ***!
    \**********************************************/

  /*! exports provided: default */

  /***/
  function srcAppMainHomeHomeComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".container-fluid {\r\n    overflow-x: hidden;\r\n}\r\n\r\nimg {\r\n    width: 203px;\r\n    height: 88px;\r\n}\r\n\r\n.black {\r\n    background-image: url('sw-space.jpg');\r\n}\r\n\r\n#pages {\r\n    border: none;\r\n}\r\n\r\n#btnPrevious:hover,\r\n#btnNext:hover {\r\n    color: white !important;\r\n    background-color: #343a40 !important;\r\n    cursor: pointer;\r\n}\r\n\r\n.cursor-pointer {\r\n    cursor: pointer !important;\r\n}\r\n\r\n.on-hover:hover {\r\n    background-color: white;\r\n}\r\n\r\n.on-hover:hover label {\r\n    color: black !important;\r\n}\r\n\r\n@media(max-width: 575px) {\r\n    .nav-padding {\r\n        padding-left: 3rem;\r\n        padding-right: 3rem;\r\n    }\r\n}\r\n\r\n@media(min-width: 576px) {\r\n    .nav-padding {\r\n        padding-left: .25rem;\r\n        padding-right: .25rem;\r\n    }\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWFpbi9ob21lL2hvbWUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGtCQUFrQjtBQUN0Qjs7QUFFQTtJQUNJLFlBQVk7SUFDWixZQUFZO0FBQ2hCOztBQUVBO0lBQ0kscUNBQXlEO0FBQzdEOztBQUVBO0lBQ0ksWUFBWTtBQUNoQjs7QUFFQTs7SUFFSSx1QkFBdUI7SUFDdkIsb0NBQW9DO0lBQ3BDLGVBQWU7QUFDbkI7O0FBRUE7SUFDSSwwQkFBMEI7QUFDOUI7O0FBRUE7SUFDSSx1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSSx1QkFBdUI7QUFDM0I7O0FBRUE7SUFDSTtRQUNJLGtCQUFrQjtRQUNsQixtQkFBbUI7SUFDdkI7QUFDSjs7QUFFQTtJQUNJO1FBQ0ksb0JBQW9CO1FBQ3BCLHFCQUFxQjtJQUN6QjtBQUNKIiwiZmlsZSI6InNyYy9hcHAvbWFpbi9ob21lL2hvbWUuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jb250YWluZXItZmx1aWQge1xyXG4gICAgb3ZlcmZsb3cteDogaGlkZGVuO1xyXG59XHJcblxyXG5pbWcge1xyXG4gICAgd2lkdGg6IDIwM3B4O1xyXG4gICAgaGVpZ2h0OiA4OHB4O1xyXG59XHJcblxyXG4uYmxhY2sge1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1nL3N3LXNwYWNlLmpwZycpO1xyXG59XHJcblxyXG4jcGFnZXMge1xyXG4gICAgYm9yZGVyOiBub25lO1xyXG59XHJcblxyXG4jYnRuUHJldmlvdXM6aG92ZXIsXHJcbiNidG5OZXh0OmhvdmVyIHtcclxuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzM0M2E0MCAhaW1wb3J0YW50O1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4uY3Vyc29yLXBvaW50ZXIge1xyXG4gICAgY3Vyc29yOiBwb2ludGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5vbi1ob3Zlcjpob3ZlciB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLm9uLWhvdmVyOmhvdmVyIGxhYmVsIHtcclxuICAgIGNvbG9yOiBibGFjayAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5AbWVkaWEobWF4LXdpZHRoOiA1NzVweCkge1xyXG4gICAgLm5hdi1wYWRkaW5nIHtcclxuICAgICAgICBwYWRkaW5nLWxlZnQ6IDNyZW07XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogM3JlbTtcclxuICAgIH1cclxufVxyXG5cclxuQG1lZGlhKG1pbi13aWR0aDogNTc2cHgpIHtcclxuICAgIC5uYXYtcGFkZGluZyB7XHJcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAuMjVyZW07XHJcbiAgICAgICAgcGFkZGluZy1yaWdodDogLjI1cmVtO1xyXG4gICAgfVxyXG59Il19 */";
    /***/
  },

  /***/
  "./src/app/main/home/home.component.ts":
  /*!*********************************************!*\
    !*** ./src/app/main/home/home.component.ts ***!
    \*********************************************/

  /*! exports provided: HomeComponent */

  /***/
  function srcAppMainHomeHomeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeComponent", function () {
      return HomeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var src_app_core_services_events_events_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! src/app/core/services/events/events.service */
    "./src/app/core/services/events/events.service.ts");
    /* harmony import */


    var src_app_core_model_categories_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/app/core/model/categories.model */
    "./src/app/core/model/categories.model.ts");
    /* harmony import */


    var src_app_core_services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! src/app/core/services/swapi/swapi.service */
    "./src/app/core/services/swapi/swapi.service.ts");
    /* harmony import */


    var src_app_core_model_page_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! src/app/core/model/page.model */
    "./src/app/core/model/page.model.ts");
    /* harmony import */


    var src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! src/environments/environment.prod */
    "./src/environments/environment.prod.ts");
    /* harmony import */


    var src_app_core_model_page_results__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! src/app/core/model/page_results */
    "./src/app/core/model/page_results.ts");

    var HomeComponent =
    /*#__PURE__*/
    function () {
      //Construtor com as suas dependências
      function HomeComponent(eventsService, swapiService) {
        _classCallCheck(this, HomeComponent);

        this.eventsService = eventsService;
        this.swapiService = swapiService;
      }
      /**
       * Inicializando as variáveis e Adicionando o primeiro evento com o id igual a zero
       */


      _createClass(HomeComponent, [{
        key: "ngOnInit",
        value: function ngOnInit() {
          var _this26 = this;

          this.categoryPages = new src_app_core_model_page_model__WEBPACK_IMPORTED_MODULE_5__["PageModel"]();
          this.showCategory = false;
          this.urlSelectedItem = "";
          this.idSelectedCategory = 0;
          this.showErrorMenssage = false;
          this.countMaxPage = 0;
          this.currentPage = 0;
          this.txtSearch = "";
          this.subscription = this.swapiService.getFromAPI("".concat(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].urlAPI, "categorypage/")).subscribe(function (responseCategories) {
            var lstCategories = [];
            Object.keys(responseCategories).forEach(function (category) {
              lstCategories.push(new src_app_core_model_categories_model__WEBPACK_IMPORTED_MODULE_3__["CategoriesModel"](responseCategories[category].txtcategorypage, responseCategories[category].idcategorypage));
            });
            lstCategories = lstCategories.sort(function (a, b) {
              if (a.txcategorypage < b.txcategorypage) {
                return -1;
              }

              if (a.txcategorypage > b.txcategorypage) {
                return 1;
              }

              return 0;
            });
            _this26.lstCategories = lstCategories;

            _this26.searchPage("".concat(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].urlAPI).concat(_this26.lstCategories[_this26.idSelectedCategory].txcategorypage, "/"));
          });
        }
      }, {
        key: "ngOnDestroy",
        value: function ngOnDestroy() {
          this.subscription.unsubscribe();
        }
        /**
         * Método responsável por informar que uma categoria foi selecionada
         * @param indexCategory
         */

      }, {
        key: "changeSelectedCategory",
        value: function changeSelectedCategory(indexCategory) {
          this.idSelectedCategory = indexCategory;
          this.categoryPages = new src_app_core_model_page_model__WEBPACK_IMPORTED_MODULE_5__["PageModel"]();
          this.currentPage = 0;
          this.txtSearch = "";
          this.clickElement("btnCollapse");
          this.searchPage("".concat(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].urlAPI).concat(this.lstCategories[this.idSelectedCategory].txcategorypage, "/"));
        }
      }, {
        key: "searchPage",
        value: function searchPage(url) {
          var _this27 = this;

          var isByName = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
          this.subscription = this.swapiService.getFromAPI(url).subscribe(function (responsePage) {
            var getType = _this27.idSelectedCategory == 0 ? !isByName ? 'getfilmpage' : 'searchfilmpagebytitle' : _this27.idSelectedCategory == 1 ? !isByName ? 'getpeoplepage' : 'searchpeoplepagebyname' : _this27.idSelectedCategory == 2 ? !isByName ? 'getplanetpage' : 'searchplanetpagebyname' : _this27.idSelectedCategory == 3 ? !isByName ? 'getspeciespage' : 'searchspeciespagebyname' : _this27.idSelectedCategory == 4 ? !isByName ? 'getstarshippage' : 'searchstarshippagebyname' : !isByName ? 'getvehiclepage' : 'searchvehiclepagebyname';
            _this27.categoryPages = responsePage[0][getType][0];
            _this27.countMaxPage = Math.ceil(responsePage[0][getType][0].count / 10);

            _this27.displayItem(responsePage[0][getType][0]['results'][0].url);
          });
        }
      }, {
        key: "displayItem",
        value: function displayItem(urlItem) {
          this.urlSelectedItem = urlItem;
          this.clickElement("btnCollapsePage");
        }
      }, {
        key: "previousPage",
        value: function previousPage() {
          var previousUrl = this.categoryPages.previous;
          this.currentPage = this.currentPage - 1 <= 0 ? 0 : this.currentPage - 1;
          this.searchPage(previousUrl, previousUrl.includes('&'));
        }
      }, {
        key: "nextPage",
        value: function nextPage() {
          var nextUrl = this.categoryPages.next;
          this.currentPage = this.currentPage + 1 > this.countMaxPage ? this.countMaxPage : this.currentPage + 1;
          this.searchPage(nextUrl, nextUrl.includes('&'));
        }
      }, {
        key: "search",
        value: function search() {
          var urlSearch = this.txtSearch == '' ? "".concat(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].urlAPI).concat(this.lstCategories[this.idSelectedCategory].txcategorypage, "/") : this.idSelectedCategory == 0 ? "".concat(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].urlAPI).concat(this.lstCategories[this.idSelectedCategory].txcategorypage, "/?title='").concat(this.txtSearch, "'") : "".concat(src_environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].urlAPI).concat(this.lstCategories[this.idSelectedCategory].txcategorypage, "/?name='").concat(this.txtSearch, "'");
          this.categoryPages = new src_app_core_model_page_model__WEBPACK_IMPORTED_MODULE_5__["PageModel"]();
          this.currentPage = 0;
          this.searchPage(urlSearch, this.txtSearch == '' ? false : true);
        }
      }, {
        key: "showElementFromCategory",
        value: function showElementFromCategory() {
          var element = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new src_app_core_model_page_results__WEBPACK_IMPORTED_MODULE_7__["PageResults"]();
          var indexCategory = -1;
          var pageResults = [];
          pageResults.push(element);
          this.lstCategories.forEach(function (category, index, array) {
            if (element.url.includes(category.txcategorypage)) {
              indexCategory = index;
            }
          });

          if (indexCategory != -1) {
            this.idSelectedCategory = indexCategory;
            this.countMaxPage = 1;
            this.categoryPages = new src_app_core_model_page_model__WEBPACK_IMPORTED_MODULE_5__["PageModel"](1, 'null', 'null', pageResults);
            this.displayItem(element.url);
          }
        }
      }, {
        key: "clickElement",
        value: function clickElement(id) {
          if (window.innerWidth < 576) {
            var element = document.getElementById(id);
            element.click();
          }
        }
      }]);

      return HomeComponent;
    }();

    HomeComponent.ctorParameters = function () {
      return [{
        type: src_app_core_services_events_events_service__WEBPACK_IMPORTED_MODULE_2__["EventsService"]
      }, {
        type: src_app_core_services_swapi_swapi_service__WEBPACK_IMPORTED_MODULE_4__["SWAPIService"]
      }];
    };

    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/main/home/home.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home.component.css */
      "./src/app/main/home/home.component.css")).default]
    })], HomeComponent);
    /***/
  },

  /***/
  "./src/app/main/main-routing.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/main/main-routing.module.ts ***!
    \*********************************************/

  /*! exports provided: MainRoutingModule */

  /***/
  function srcAppMainMainRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainRoutingModule", function () {
      return MainRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _home_home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./home/home.component */
    "./src/app/main/home/home.component.ts");

    var routes = [{
      path: 'home',
      component: _home_home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"]
    }];

    var MainRoutingModule = function MainRoutingModule() {
      _classCallCheck(this, MainRoutingModule);
    };

    MainRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], MainRoutingModule);
    /***/
  },

  /***/
  "./src/app/main/main.module.ts":
  /*!*************************************!*\
    !*** ./src/app/main/main.module.ts ***!
    \*************************************/

  /*! exports provided: MainModule */

  /***/
  function srcAppMainMainModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MainModule", function () {
      return MainModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _main_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./main-routing.module */
    "./src/app/main/main-routing.module.ts");
    /* harmony import */


    var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./home/home.component */
    "./src/app/main/home/home.component.ts");
    /* harmony import */


    var _core_core_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../core/core.module */
    "./src/app/core/core.module.ts");
    /* harmony import */


    var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @fortawesome/angular-fontawesome */
    "./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js");
    /* harmony import */


    var _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! @ngx-loading-bar/core */
    "./node_modules/@ngx-loading-bar/core/fesm2015/ngx-loading-bar-core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");

    var MainModule = function MainModule() {
      _classCallCheck(this, MainModule);
    };

    MainModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"]],
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _main_routing_module__WEBPACK_IMPORTED_MODULE_3__["MainRoutingModule"], _core_core_module__WEBPACK_IMPORTED_MODULE_5__["CoreModule"], _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_6__["FontAwesomeModule"], _ngx_loading_bar_core__WEBPACK_IMPORTED_MODULE_7__["LoadingBarModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]]
    })], MainModule);
    /***/
  },

  /***/
  "./src/environments/environment.prod.ts":
  /*!**********************************************!*\
    !*** ./src/environments/environment.prod.ts ***!
    \**********************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentProdTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");

    var environment = {
      production: true,
      urlContent: "../../../assets/config/",
      // urlAPI: "http://localhost:80/"
      urlAPI: "https://swapi-backend.herokuapp.com/"
    };
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    var environment = {
      production: false,
      urlContent: "../../../assets/config/"
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"]).catch(function (err) {
      return console.error(err);
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! C:\Users\odilomar.rocha\Desktop\git\angular-challenge\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map