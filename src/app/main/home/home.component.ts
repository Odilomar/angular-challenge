import { Component, OnInit, OnDestroy } from '@angular/core';
import { EventsService } from 'src/app/core/services/events/events.service';
import { CategoriesModel } from 'src/app/core/model/categories.model';
import { Subscription } from 'rxjs';
import { SWAPIService } from 'src/app/core/services/swapi/swapi.service';
import { PageModel } from 'src/app/core/model/page.model';
import { environment } from 'src/environments/environment.prod';
import { PageResults } from 'src/app/core/model/page_results';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {

  //Instanciando as variáveis
  public showCategory: boolean;
  public urlSelectedItem: string;
  public idSelectedCategory: number;
  public showErrorMenssage: boolean;
  public categoryPages: PageModel;
  public lstCategories: CategoriesModel[];
  public countMaxPage: number;
  public currentPage: number;
  public txtSearch: string;
  public winWidth: number;
  private subscription: Subscription;

  //Construtor com as suas dependências
  constructor(
    private eventsService: EventsService,
    private swapiService: SWAPIService
  ) { }

  /**
   * Inicializando as variáveis e Adicionando o primeiro evento com o id igual a zero
   */
  ngOnInit() {
    this.categoryPages = new PageModel();
    this.showCategory = false;
    this.urlSelectedItem = "";
    this.idSelectedCategory = 0;
    this.showErrorMenssage = false;
    this.countMaxPage = 0;
    this.currentPage = 0;
    this.txtSearch = "";
    this.winWidth = window.innerWidth;

    this.subscription = this.swapiService.getFromAPI(`${environment.urlAPI}categorypage/`).subscribe(
      (responseCategories) => {
        let lstCategories: CategoriesModel[] = [];

        Object.keys(responseCategories).forEach(category => {
          lstCategories.push(new CategoriesModel(responseCategories[category].txtcategorypage, responseCategories[category].idcategorypage));
        });

        lstCategories = lstCategories.sort((a, b) => {
          if (a.txcategorypage < b.txcategorypage) { return -1; }
          if (a.txcategorypage > b.txcategorypage) { return 1; }
          return 0;
        });

        this.lstCategories = lstCategories;
        this.searchPage(`${environment.urlAPI}${this.lstCategories[this.idSelectedCategory].txcategorypage}/`);
      }
    );
  }

  ngOnDestroy() { this.subscription.unsubscribe(); }

  /**
   * Método responsável por informar que uma categoria foi selecionada
   * @param indexCategory 
   */
  public changeSelectedCategory(indexCategory: number) {
    this.idSelectedCategory = indexCategory;
    this.categoryPages = new PageModel();
    this.currentPage = 0;
    this.txtSearch = "";

    this.clickElement("btnCollapse");

    this.searchPage(`${environment.urlAPI}${this.lstCategories[this.idSelectedCategory].txcategorypage}/`);
  }

  private searchPage(url: string, isByName: boolean = false) {
    this.subscription = this.swapiService.getFromAPI(url).subscribe(
      (responsePage: PageModel) => {
        let getType: string = this.idSelectedCategory == 0 ? !isByName ? 'getfilmpage' : 'searchfilmpagebytitle' : this.idSelectedCategory == 1 ? !isByName ? 'getpeoplepage' : 'searchpeoplepagebyname' : this.idSelectedCategory == 2 ? !isByName ? 'getplanetpage' : 'searchplanetpagebyname' : this.idSelectedCategory == 3 ? !isByName ? 'getspeciespage' : 'searchspeciespagebyname' : this.idSelectedCategory == 4 ? !isByName ? 'getstarshippage' : 'searchstarshippagebyname' : !isByName ? 'getvehiclepage' : 'searchvehiclepagebyname';

        this.categoryPages = responsePage[0][getType][0];
        this.countMaxPage = Math.ceil(responsePage[0][getType][0].count / 10);

        this.displayItem(responsePage[0][getType][0]['results'][0].url);
      }
    );
  }

  public displayItem(urlItem: string) {
    this.urlSelectedItem = urlItem;    
  }

  public previousPage() {
    let previousUrl: string = this.categoryPages.previous;
    this.currentPage = this.currentPage - 1 <= 0 ? 0 : this.currentPage - 1;
    this.searchPage(previousUrl, previousUrl.includes('&'));
  }

  public nextPage() {
    let nextUrl: string = this.categoryPages.next;
    this.currentPage = this.currentPage + 1 > this.countMaxPage ? this.countMaxPage : this.currentPage + 1;
    this.searchPage(nextUrl, nextUrl.includes('&'));
  }

  public search() {
    let urlSearch: string = this.txtSearch == '' ? `${environment.urlAPI}${this.lstCategories[this.idSelectedCategory].txcategorypage}/` : this.idSelectedCategory == 0 ? `${environment.urlAPI}${this.lstCategories[this.idSelectedCategory].txcategorypage}/?title='${this.txtSearch}'` : `${environment.urlAPI}${this.lstCategories[this.idSelectedCategory].txcategorypage}/?name='${this.txtSearch}'`;
    this.categoryPages = new PageModel();
    this.currentPage = 0;

    this.searchPage(urlSearch, this.txtSearch == '' ? false : true);
  }

  public showElementFromCategory(element: PageResults = new PageResults()) {
    let indexCategory: number = -1;
    let pageResults: PageResults[] = [];

    pageResults.push(element);

    this.lstCategories.forEach((category, index, array) => {
      if (element.url.includes(category.txcategorypage)) {
        indexCategory = index;
      }
    })

    if (indexCategory != -1) {
      this.idSelectedCategory = indexCategory;
      this.countMaxPage = 1;
      this.categoryPages = new PageModel(1, 'null', 'null', pageResults);

      this.displayItem(element.url);
    }

    this.txtSearch = '';
  }

  private clickElement(id: string){
    if (window.innerWidth < 576) {
      let element: HTMLElement = document.getElementById(id) as HTMLElement;
      element.click();
    }
  }
}
