import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { PeopleComponent } from './layout/people/people.component';
import { LoadingBarHttpClientModule } from '@ngx-loading-bar/http-client';
import { FilmComponent } from './layout/film/film.component';
import { StarshipComponent } from './layout/starship/starship.component';
import { PlanetComponent } from './layout/planet/planet.component';
import { VehicleComponent } from './layout/vehicle/vehicle.component';
import { SpeciesComponent } from './layout/species/species.component';

@NgModule({
  declarations: [PeopleComponent, FilmComponent, StarshipComponent, PlanetComponent, VehicleComponent, SpeciesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    LoadingBarHttpClientModule
  ],
  exports: [PeopleComponent, FilmComponent, StarshipComponent, PlanetComponent, VehicleComponent, SpeciesComponent]
})
export class CoreModule { }
