import { PeopleModel } from './people.model';
import { PlanetsModel } from './planets.model';
import { StarshipModel } from './starship.model';
import { VehiclesModel } from './vehicles.model';
import { SpeciesModel } from './species.model';

export class FilmsModule {
    public title: string;
    public episode_id: number;
    public opening_crawl: string;
    public director: string;
    public producer: string;
    public release_date: string;
    public characters: string[] | PeopleModel[];
    public planets: string[] | PlanetsModel[];
    public starships: string[] | StarshipModel[];
    public vehicles: string[] | VehiclesModel[];
    public species: string[] | SpeciesModel[];
    public created: string;
    public edited: string;
    public url: string;

    constructor(
        title: string = "",
        episode_id: number = 0,
        opening_crawl: string = "",
        director: string = "",
        producer: string = "",
        release_date: string = "",
        characters: string[] | PeopleModel[] = [],
        planets: string[] | PlanetsModel[] = [],
        starships: string[] | StarshipModel[] = [],
        vehicles: string[] | VehiclesModel[] = [],
        species: string[] | SpeciesModel[] = [],
        created: string = "",
        edited: string = "",
        url: string = ""
    ){
        this.title = title;
        this.episode_id = episode_id;
        this.opening_crawl = opening_crawl;
        this.director = director;
        this.producer = producer;
        this.release_date = release_date;
        this.characters = characters;
        this.planets = planets;
        this.starships = starships;
        this.vehicles = vehicles;
        this.species = species;
        this.created = created;
        this.edited = edited;
        this.url = url;
    }
}