import { PeopleModel } from './people.model';
import { FilmsModule } from './films.model';
import { PlanetsModel } from './planets.model';

export class SpeciesModel {
    public name: string;
    public classification: string;
    public designation: string;
    public average_height: string;
    public skin_color: string;
    public hair_color: string;
    public eye_color: string;
    public average_lifespan: string;
    public homeworld: string | PlanetsModel;
    public language: string;
    public people: string[] | PeopleModel[];
    public films: string[] | FilmsModule[];
    public created: string;
    public edited: string;
    public url: string;

    constructor(
        name: string = "",
        classification: string = "",
        designation: string = "",
        average_height: string = "",
        skin_color: string = "",
        hair_color: string = "",
        eye_color: string = "",
        average_lifespan: string = "",
        homeworld: string | PlanetsModel = "",
        language: string = "",
        people: string[] | PeopleModel[] = [],
        films: string[] | FilmsModule[] = [],
        created: string = "",
        edited: string = "",
        url: string = ""
    ) {
        this.name = name;
        this.classification = classification;
        this.designation = designation;
        this.average_height = average_height;
        this.skin_color = skin_color;
        this.hair_color = hair_color;
        this.eye_color = eye_color;
        this.average_lifespan = average_lifespan;
        this.homeworld = homeworld;
        this.language = language;
        this.people = people;
        this.films = films;
        this.created = created;
        this.edited = edited;
        this.url = url;
    }
}