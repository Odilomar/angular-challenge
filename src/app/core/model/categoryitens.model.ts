export class CategoryItens{
    public idCategory: number;
    public url: string;

    constructor(
        idCategory: number = 0,
        url: string = ""
    ){
        this.idCategory = idCategory;
        this.url = url;
    }
}