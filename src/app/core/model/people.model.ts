import { FilmsModule } from './films.model';
import { SpeciesModel } from './species.model';
import { VehiclesModel } from './vehicles.model';
import { StarshipModel } from './starship.model';
import { PlanetsModel } from './planets.model';

export class PeopleModel{
        public name: string;
        public height: string;
        public mass: string;
        public hair_color: string;
        public skin_color: string;
        public eye_color: string;
        public birth_year: string;
        public gender: string;
        public homeworld: string | PlanetsModel;
        public films: string[] | FilmsModule[];
        public species: string[] | SpeciesModel[];
        public vehicles: string[] | VehiclesModel[];
        public starships: string[] | StarshipModel[];
        public created: string;
        public edited: string;
        public url: string;

        constructor(
            name: string = "",
            height: string  = "",
            mass: string  = "",
            hair_color: string  = "",
            skin_color: string  = "",
            eye_color: string  = "",
            birth_year: string  = "",
            gender: string  = "",
            homeworld: string  = "",
            films: string[] | FilmsModule[] = [],
            species: string[] | SpeciesModel[] = [],
            vehicles: string[] | VehiclesModel[] = [],
            starships: string[] | StarshipModel[] = [],
            created: string  = "",
            edited: string  = "",
            url: string  = ""
        ){
            this.name = name;
            this.height = height;
            this.mass = mass;
            this.hair_color = hair_color;
            this.skin_color = skin_color;
            this.eye_color = eye_color;
            this.birth_year = birth_year;
            this.gender = gender;
            this.homeworld = homeworld;
            this.films = films;
            this.species = species;
            this.vehicles = vehicles;
            this.starships = starships;
            this.created = created;
            this.edited = edited;
            this.url = url;
        }
}