export class CategoriesModel{
    public txcategorypage: string;
    private idCategorypage: number;

    constructor(
        txcategorypage: string = "",
        idCategorypage: number = 0
    ){
        this.txcategorypage = txcategorypage;
        this.idCategorypage = idCategorypage;
    }

    public getIdCategorypage(){
        return this.idCategorypage;
    }
}