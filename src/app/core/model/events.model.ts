export class EventsModel{
    public log: string;
    public id: number;

    constructor(
        log: string = "",
        id: number = 0
    ){
        this.log = log;
        this.id = id;
    }
}