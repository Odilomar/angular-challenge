import { PageResults } from './page_results';

export class PageModel{
    public count: number;
    public next: string;
    public previous: string;
    public results: PageResults[];

    constructor(
        count: number = 0,
        next: string = "",
        previous: string = null,
        results: PageResults[] = []
    ){
        this.count = count;
        this.next = next;
        this.previous = previous;
        this.results = results;
    }
}