import { PeopleModel } from './people.model';
import { FilmsModule } from './films.model';

export class VehiclesModel {
    public name: string;
    public model: string;
    public manufacturer: string;
    public cost_in_credits: string;
    public length: string;
    public max_atmosphering_speed: string;
    public crew: string;
    public passengers: string;
    public cargo_capacity: string;
    public consumables: string;
    public vehicle_class: string;
    public pilots: string[] | PeopleModel[];
    public films: string[] | FilmsModule[];
    public created: string;
    public edited: string;
    public url: string;

    constructor(
        name: string = "",
        model: string = "",
        manufacturer: string = "",
        cost_in_credits: string = "",
        length: string = "",
        max_atmosphering_speed: string = "",
        crew: string = "",
        passengers: string = "",
        cargo_capacity: string = "",
        consumables: string = "",
        vehicle_class: string = "",
        pilots: string[] | PeopleModel[] = [],
        films: string[] | FilmsModule[] = [],
        created: string = "",
        edited: string = "",
        url: string = ""
    ){
        this.name = name;
        this.model = model;
        this.manufacturer = manufacturer;
        this.cost_in_credits = cost_in_credits;
        this.length = length;
        this.max_atmosphering_speed = max_atmosphering_speed;
        this.crew = crew;
        this.passengers = passengers;
        this.cargo_capacity = cargo_capacity;
        this.consumables = consumables;
        this.vehicle_class = vehicle_class;
        this.pilots = pilots;
        this.films = films;
        this.created = created;
        this.edited = edited;
        this.url = url;
    }
}