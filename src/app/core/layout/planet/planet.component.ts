import { Component, OnInit, OnDestroy, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { PlanetsModel } from '../../model/planets.model';
import { SWAPIService } from '../../services/swapi/swapi.service';
import { Subscription } from 'rxjs';
import { FilmsModule } from '../../model/films.model';
import { PeopleModel } from '../../model/people.model';
import { PageResults } from '../../model/page_results';

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrls: ['./planet.component.css']
})
export class PlanetComponent implements OnInit, OnChanges, OnDestroy {

  @Input() urlSelectedItem: string;
  @Output() urlElementFromCategory = new EventEmitter<PageResults>();

  /**
   * Instanciando a variável
   */
  public planet: PlanetsModel;
  private subscription: Subscription;

  /**
   * Injetando a dependência
   * @param swapiService 
   */
  constructor(
    private swapiService: SWAPIService
  ) { }

  /**
   * Inicializando a variável
   */
  ngOnInit() {
    this.subscription = new Subscription();
    this.planet = new PlanetsModel();

    if (this.urlSelectedItem.includes("planets")) {
      this.checkPlanet();      
    }
  }

  /**
   * Atualizando a variável
   */
  ngOnChanges() {
    if(this.planet){
      if (this.planet.url != this.urlSelectedItem && this.urlSelectedItem.includes("planets")) {
        this.checkPlanet();
      }
    }
  }  

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private checkPlanet() {
    this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(
      (response) => {
        this.planet = response[0]['getplanet'][0];

        if (typeof this.planet.residents[0] == "string") {
          this.searchResidents();      
        }
    
        if(typeof this.planet.films[0] == "string") {
          this.searchFilms();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private searchFilms() {
    this.planet.films.forEach(filmsItem => {
      this.subscription = this.swapiService.getFromAPI(filmsItem).subscribe(
        (response: FilmsModule) => {
          this.planet.films[this.planet.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
        }
      );
    });
  }

  private searchResidents() {
    this.planet.residents.forEach(residentItem => {
      this.subscription = this.swapiService.getFromAPI(residentItem).subscribe(
        (response: PeopleModel) => {
          this.planet.residents[this.planet.residents.indexOf(residentItem)] = response[0]['getpeople'][0];
        }
      );
    });
  }

  public returnElementToHome(elementName: string, elementUrl: string) { this.urlElementFromCategory.emit(new PageResults(elementName, elementUrl)); }

}
