import { Component, OnInit, OnDestroy, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { FilmsModule } from '../../model/films.model';
import { SWAPIService } from '../../services/swapi/swapi.service';
import { Subscription } from 'rxjs';
import { PeopleModel } from '../../model/people.model';
import { PlanetsModel } from '../../model/planets.model';
import { StarshipModel } from '../../model/starship.model';
import { VehiclesModel } from '../../model/vehicles.model';
import { SpeciesModel } from '../../model/species.model';
import { PageResults } from '../../model/page_results';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent implements OnInit, OnDestroy, OnChanges {

  @Input() urlSelectedItem: string;
  @Output() urlElementFromCategory = new EventEmitter<PageResults>();

  /**
   * Instanciando a variável
   */
  public film: FilmsModule;
  private subscription: Subscription;

  /**
   * Injetando a dependência
   * @param swapiService 
   */
  constructor(
    private swapiService: SWAPIService
  ) { }

  /**
   * Inicializando a variável
   */
  ngOnInit() {
    this.subscription = new Subscription();
    this.film = new FilmsModule();
    if (this.urlSelectedItem.includes("films")) {
      this.checkFilm();      
    }
  }

  ngOnChanges(){
    if (this.film) {
      if (this.film.url != this.urlSelectedItem && this.urlSelectedItem.includes("films")) {
        this.checkFilm()
      }
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  private checkFilm() {    
    this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(
      (response) => {
        this.film = response[0]['getfilm'][0];

        if(typeof this.film.characters[0] == "string"){
          this.searchCharacters();
        }
    
        if(typeof this.film.planets[0] == "string"){
          this.searchPlanets();
        }
    
        if(typeof this.film.species[0] == "string"){
          this.searchSpecies();
        }
    
        if(typeof this.film.starships[0] == "string"){
          this.searchStarships();
        }
    
        if(typeof this.film.vehicles[0] == "string"){
          this.searchVehicles();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private searchCharacters() {
    this.film.characters.forEach(characterItem => {
      this.subscription = this.swapiService.getFromAPI(characterItem).subscribe(
        (response: PeopleModel) => {
          this.film.characters[this.film.characters.indexOf(characterItem)] = response[0]['getpeople'][0];
        }
      );
    });
  }

  private searchPlanets() {
    this.film.planets.forEach(planetsItem => {
      this.subscription = this.swapiService.getFromAPI(planetsItem).subscribe(
        (response: PlanetsModel) => {
          this.film.planets[this.film.planets.indexOf(planetsItem)] = response[0]['getplanet'][0];
        }
      );
    });
  }

  private searchStarships() {
    this.film.starships.forEach(starshipsItem => {
      this.subscription = this.swapiService.getFromAPI(starshipsItem).subscribe(
        (response: StarshipModel) => {
          this.film.starships[this.film.starships.indexOf(starshipsItem)] = response[0]['getstarship'][0];
        }
      );
    });
  }

  private searchVehicles() {
    this.film.vehicles.forEach(vehiclesItem => {
      this.subscription = this.swapiService.getFromAPI(vehiclesItem).subscribe(
        (response: VehiclesModel) => {
          this.film.vehicles[this.film.vehicles.indexOf(vehiclesItem)] = response[0]['getvehicle'][0];
        }
      );
    });
  }

  private searchSpecies() {
    this.film.species.forEach(speciesItem => {
      this.subscription = this.swapiService.getFromAPI(speciesItem).subscribe(
        (response: SpeciesModel) => {
          this.film.species[this.film.species.indexOf(speciesItem)] = response[0]['getspecies'][0];
        }
      );
    });
  }

  public changeItem(item: string){ this.urlSelectedItem = item; }

  public returnElementToHome(elementName: string, elementUrl: string) { this.urlElementFromCategory.emit(new PageResults(elementName, elementUrl)); }
}
