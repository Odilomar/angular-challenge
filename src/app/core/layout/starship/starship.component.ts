import { Component, OnInit, OnDestroy, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { StarshipModel } from '../../model/starship.model';
import { SWAPIService } from '../../services/swapi/swapi.service';
import { Subscription } from 'rxjs';
import { FilmsModule } from '../../model/films.model';
import { PeopleModel } from '../../model/people.model';
import { PageResults } from '../../model/page_results';

@Component({
  selector: 'app-starship',
  templateUrl: './starship.component.html',
  styleUrls: ['./starship.component.css']
})
export class StarshipComponent implements OnInit, OnChanges, OnDestroy {

  @Input() urlSelectedItem: string;
  @Output() urlElementFromCategory = new EventEmitter<PageResults>();

  /**
    * Instanciando a variável
    */
  public starship: StarshipModel;
  private subscription: Subscription;

  /**
    * Injetando a dependência
    * @param swapiServices 
   */
  constructor(
    private swapiService: SWAPIService
  ) { }

  /**
    * Inicializando a variável
    */
  ngOnInit() {
    this.subscription = new Subscription();
    this.starship = new StarshipModel();
    if (this.urlSelectedItem.includes("starships")) {
      this.checkStarship();      
    }
  }

  /**
    * Atualizando a variável
    */
  ngOnChanges() {
    if (this.starship) {
      if (this.starship.url != this.urlSelectedItem && this.urlSelectedItem.includes("starships")) {
        this.checkStarship();
      }
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  private checkStarship(){
    this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(
      (response) => {
        this.starship = response[0]['getstarship'][0];

        if (typeof this.starship.pilots[0] == "string") {
          this.searchPilots();      
        }
    
        if (typeof this.starship.films[0] == "string") {
          this.searchFilms();      
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private searchFilms() {
    this.starship.films.forEach(filmsItem => {
      this.subscription = this.swapiService.getFromAPI(filmsItem).subscribe(
        (response: FilmsModule) => {
          this.starship.films[this.starship.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
        }
      );
    });
  }

  private searchPilots() {
    this.starship.pilots.forEach(pilotsItem => {
      this.subscription = this.swapiService.getFromAPI(pilotsItem).subscribe(
        (response: PeopleModel) => {
          this.starship.pilots[this.starship.pilots.indexOf(pilotsItem)] = response[0]['getpeople'][0];
        }
      );
    });
  }

  public returnElementToHome(elementName: string, elementUrl: string) { this.urlElementFromCategory.emit(new PageResults(elementName, elementUrl)); }

}
