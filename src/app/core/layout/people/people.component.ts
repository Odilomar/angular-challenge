import { Component, OnInit, OnDestroy, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { PeopleModel } from '../../model/people.model';
import { SWAPIService } from '../../services/swapi/swapi.service';
import { Subscription } from 'rxjs';
import { StarshipModel } from '../../model/starship.model';
import { VehiclesModel } from '../../model/vehicles.model';
import { SpeciesModel } from '../../model/species.model';
import { FilmsModule } from '../../model/films.model';
import { PlanetsModel } from '../../model/planets.model';
import { PageResults } from '../../model/page_results';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.css']
})
export class PeopleComponent implements OnInit, OnChanges, OnDestroy {
  
  @Input() urlSelectedItem: string;
  @Output() urlElementFromCategory = new EventEmitter<PageResults>();

  /**
   * Instanciando a variável
   */
  public people: PeopleModel;
  private subscription: Subscription;

  /**
   * Injetando a dependência
   * @param swapiService 
   */
  constructor(
    private swapiService: SWAPIService
  ) { }

  /**
   * Inicializando a variável
   */
  ngOnInit() {
    this.subscription = new Subscription();
    this.people = new PeopleModel();
    if(this.urlSelectedItem.includes('people')){
      this.checkPeople();
    }
  }

  /**
   * Atualizando a variável
   */
  ngOnChanges(){    
    if (this.people) {
      if(this.people.url != this.urlSelectedItem && this.urlSelectedItem.includes('people')){
        this.checkPeople();
      }      
    }
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  private checkPeople(){
    this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(
      (response) => {
        this.people = response[0]['getpeople'][0];          

        if (typeof this.people.homeworld == "string") {
          this.searchHomeword();      
        }
    
        if (typeof this.people.films[0] == "string") {
          this.searchFilms();      
        }
    
        if (typeof this.people.species[0] == "string") {
          this.searchSpecies();      
        }
    
        if(typeof this.people.starships[0] == "string") {
          this.searchStarships();
        }
    
        if(typeof this.people.vehicles[0] == "string")  {
          this.searchVehicles();
        }   
      }
    );     
  }

  private searchHomeword(){
    let urlHomeword = typeof this.people.homeworld == "string" ? this.people.homeworld:this.people.homeworld.url;

    this.subscription = this.swapiService.getFromAPI(urlHomeword).subscribe(
      (response: PlanetsModel) => {
        this.people.homeworld = response[0]['getplanet'][0];
      },
      (error) => {
        console.log(error);          
      }
    );
  }

  private searchFilms() {
    this.people.films.forEach(filmsItem => {
      this.subscription = this.swapiService.getFromAPI(filmsItem).subscribe(
        (response: FilmsModule) => {
          this.people.films[this.people.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
        }
      );
    });
  }

  private searchStarships() {
    this.people.starships.forEach(starshipsItem => {
      this.subscription = this.swapiService.getFromAPI(starshipsItem).subscribe(
        (response: StarshipModel) => {
          this.people.starships[this.people.starships.indexOf(starshipsItem)] = response[0]['getstarship'][0];
        }
      );
    });
  }

  private searchVehicles() {
    this.people.vehicles.forEach(vehiclesItem => {
      this.subscription = this.swapiService.getFromAPI(vehiclesItem).subscribe(
        (response: VehiclesModel) => {
          this.people.vehicles[this.people.vehicles.indexOf(vehiclesItem)] = response[0]['getvehicle'][0];
        }
      );
    });
  }

  private searchSpecies() {
    this.people.species.forEach(speciesItem => {
      this.subscription = this.swapiService.getFromAPI(speciesItem).subscribe(
        (response: SpeciesModel) => {
          this.people.species[this.people.species.indexOf(speciesItem)] = response[0]['getspecies'][0];
        }
      );
    });
  }

  public returnElementToHome(elementName: string, elementUrl: string) { this.urlElementFromCategory.emit(new PageResults(elementName, elementUrl)); }

}
