import { Component, OnInit, OnDestroy, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { VehiclesModel } from '../../model/vehicles.model';
import { SWAPIService } from '../../services/swapi/swapi.service';
import { Subscription } from 'rxjs';
import { FilmsModule } from '../../model/films.model';
import { PeopleModel } from '../../model/people.model';
import { PageResults } from '../../model/page_results';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit, OnChanges, OnDestroy {

  @Input() urlSelectedItem: string;
  @Output() urlElementFromCategory = new EventEmitter<PageResults>();

  /**
   * Instanciando a variável
   */
  public vehicle: VehiclesModel;
  private subscription: Subscription;

  /**
   * Injetando a dependência
   * @param swapiServices 
   */
  constructor(
    private swapiService: SWAPIService
  ) { }

  /**
   * Inicializando a variável
   */
  ngOnInit() {
    this.subscription = new Subscription();
    this.vehicle = new VehiclesModel();
    if (this.urlSelectedItem.includes("vehicles")) {
      this.checkVehicles();      
    }
  }

  /**
   * Atualizando a variável
   */
  ngOnChanges() {
    if (this.vehicle) {
      if (this.vehicle.url != this.urlSelectedItem && this.urlSelectedItem.includes("vehicles")) {
        this.checkVehicles();
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private checkVehicles() {
    this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(
      (response) => {
        this.vehicle = response[0]['getvehicle'][0];

        if (typeof this.vehicle.pilots[0] == "string") {
          this.searchPilots();
        }

        if (typeof this.vehicle.films[0] == "string") {
          this.searchFilms();
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private searchFilms() {
    this.vehicle.films.forEach(filmsItem => {
      this.subscription = this.swapiService.getFromAPI(filmsItem).subscribe(
        (response: FilmsModule) => {
          this.vehicle.films[this.vehicle.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
        },
        (error) => {
          console.log(error);
        }
      );
    });
  }

  private searchPilots() {
    this.vehicle.pilots.forEach(pilotsItem => {
      this.subscription = this.swapiService.getFromAPI(pilotsItem).subscribe(
        (response: PeopleModel) => {
          this.vehicle.pilots[this.vehicle.pilots.indexOf(pilotsItem)] = response[0]['getpeople'][0];
        },
        (error) => {
          console.log(error);
        }
      );
    });
  }

  public returnElementToHome(elementName: string, elementUrl: string) { this.urlElementFromCategory.emit(new PageResults(elementName, elementUrl)); }
}
