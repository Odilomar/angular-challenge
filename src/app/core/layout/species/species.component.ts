import { Component, OnInit, OnDestroy, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { SpeciesModel } from '../../model/species.model';
import { SWAPIService } from '../../services/swapi/swapi.service';
import { Subscription } from 'rxjs';
import { PlanetsModel } from '../../model/planets.model';
import { FilmsModule } from '../../model/films.model';
import { PeopleModel } from '../../model/people.model';
import { PageResults } from '../../model/page_results';

@Component({
  selector: 'app-species',
  templateUrl: './species.component.html',
  styleUrls: ['./species.component.css']
})
export class SpeciesComponent implements OnInit, OnChanges, OnDestroy {

  @Input() urlSelectedItem: string;
  @Output() urlElementFromCategory = new EventEmitter<PageResults>();
  
  /**
   * Instanciando a variável
   */
  public species: SpeciesModel;
  private subscription: Subscription;

  /**
   * Injetando a dependência
   * @param swapiService 
   */
  constructor(
    private swapiService: SWAPIService
  ) { }

  /**
   * Inicializando a variável
   */
  ngOnInit() {
    this.subscription = new Subscription();
    this.species = new SpeciesModel();
    if (this.urlSelectedItem.includes("species")) {
      this.checkSpecies();      
    }
  }

  /**
   * Atualizando a variável
   */
  ngOnChanges() {
    if (this.species) {
      if (this.species.url != this.urlSelectedItem && this.urlSelectedItem.includes("species")) {
        this.checkSpecies();
      }
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  private checkSpecies() {
    this.subscription = this.swapiService.getFromAPI(this.urlSelectedItem).subscribe(
      (response) => {
        this.species = response[0]['getspecies'][0];

        if(typeof this.species.homeworld[0] == "string") {
          this.searchHomeword();
        }
    
        if (typeof this.species.people[0] == "string") {
          this.searchPeople();      
        }
    
        if (typeof this.species.films[0] == "string") {
          this.searchFilms();      
        }
      },
      (error) => {
        console.log(error);
      }
    );
  }

  private searchHomeword(){
    let urlHomeWord: string  = this.species.homeworld[0];

    this.subscription = this.swapiService.getFromAPI(urlHomeWord).subscribe(
      (response: PlanetsModel) => {
        this.species.homeworld = response[0]['getplanet'][0];
      }
    );
  }

  private searchFilms() {
    this.species.films.forEach(filmsItem => {
      this.subscription = this.swapiService.getFromAPI(filmsItem).subscribe(
        (response: FilmsModule) => {
          this.species.films[this.species.films.indexOf(filmsItem)] = response[0]['getfilm'][0];
        }
      );
    });
  }

  private searchPeople() {
    this.species.people.forEach(peopleItem => {
      this.subscription = this.swapiService.getFromAPI(peopleItem).subscribe(
        (response: PeopleModel) => {
          this.species.people[this.species.people.indexOf(peopleItem)] = response[0]['getpeople'][0];
        }
      );
    });
  }

  public returnElementToHome(elementName: string, elementUrl: string) { this.urlElementFromCategory.emit(new PageResults(elementName, elementUrl)); }
}
