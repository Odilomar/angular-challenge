import { Injectable } from '@angular/core';
import { EventsModel } from '../../model/events.model';
import { isUndefined } from 'util';

@Injectable({
  providedIn: 'root'
})

export class EventsService {
  /**
   * Instanciando a variável
   */
  private events: EventsModel[];

  /**
   * Inicializando a variável
   */
  constructor() {
    this.events = [];
  }

  /**
   * Método responsável por adicionar um novo evento à lista
   * @param tempEvent 
   */
  public addEvent(tempEvent: EventsModel) {
    if(this.eventExists(tempEvent.log)){
      this.removeEvent(tempEvent.log);
    }

    this.events.push(tempEvent);
  }

  /**
   * Método responsável por verificar a existencia de um evento na lista
   * @param log 
   */
  public eventExists(log: string): boolean {
    let tempEvent: EventsModel[] = this.events.filter((value, index, arr) => { return value.log == log });

    return tempEvent[0] ? true : false;
  }

  /**
   * Método responsável por procurar um evento na lista e retornar seu id
   * @param log 
   */
  public returnEventId(log: string): number {
    let tempEvent: EventsModel[] = this.events.filter((value, index, arr) => { return value.log == log });
    return isUndefined(tempEvent[0]) ? -1: tempEvent[0].id;
  }

  /**
   * Método responsavel por remover evento da lista
   * @param log 
   */
  public removeEvent(log: string) {
    this.events = this.events.filter((value, index, arr) => { return value.log != log });
  }

}
