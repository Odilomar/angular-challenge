import { TestBed } from '@angular/core/testing';

import { CategoryitemService } from './categoryitem.service';

describe('CategoryitemService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CategoryitemService = TestBed.get(CategoryitemService);
    expect(service).toBeTruthy();
  });
});
