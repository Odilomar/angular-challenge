import { Injectable } from '@angular/core';
import { PeopleModel } from '../../model/people.model';
import { FilmsModule } from '../../model/films.model';
import { SpeciesModel } from '../../model/species.model';
import { VehiclesModel } from '../../model/vehicles.model';
import { StarshipModel } from '../../model/starship.model';
import { PlanetsModel } from '../../model/planets.model';
import { CategoriesModel } from '../../model/categories.model';
import { CategoryItens } from '../../model/categoryitens.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryitemService {

  /**
   * Instancia as variáveis que serão utilizadas
   */
  private urlSelectedItem: string;
  private selectedCategoryIndex: number;

  constructor() {
    this.urlSelectedItem = '';
    this.selectedCategoryIndex = 0;
   }

   /**
    * getUrlSelectedItem
    */
   public getUrlSelectedItem() {
     return this.urlSelectedItem;
   }

   /**
    * setUrlSelectedItem
    * @param url 
    */
   public setUrlSelectedItem(url: string){
     this.urlSelectedItem = url;
   }

   /**
    * getSelectedCategoryIndex
    */
   public getSelectedCategoryIndex() {
     return this.selectedCategoryIndex;
   }

   /**
    * setSelectedCategoryIndex
    */
   public setSelectedCategoryIndex(index: number) {
     this.selectedCategoryIndex = index;
   }
}
