import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainRoutingModule } from './main/main-routing.module';


const routes: Routes = [
  {path: "**", redirectTo: "home"}
];

@NgModule({
  imports: [
    MainRoutingModule,
    RouterModule.forRoot(routes)    
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
