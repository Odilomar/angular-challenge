export const environment = {
  production: true,
  urlContent: "../../../assets/config/",
  // urlAPI: "http://localhost:80/"
  urlAPI: "https://swapi-backend.herokuapp.com/"
};
